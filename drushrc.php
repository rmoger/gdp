<?php 


$options['sites'] = array (
);
$options['profiles'] = array (
  0 => 'minimal',
  1 => 'standard',
  2 => 'testing',
);
$options['packages'] = array (
  'base' => 
  array (
    'modules' => 
    array (
      'trigger_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/trigger/tests/trigger_test.module',
        'basename' => 'trigger_test.module',
        'name' => 'trigger_test',
        'info' => 
        array (
          'name' => 'Trigger Test',
          'description' => 'Support module for Trigger tests.',
          'package' => 'Testing',
          'core' => '7.x',
          'hidden' => true,
          'version' => '7.21',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'trigger' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/trigger/trigger.module',
        'basename' => 'trigger.module',
        'name' => 'trigger',
        'info' => 
        array (
          'name' => 'Trigger',
          'description' => 'Enables actions to be fired on certain system events, such as when new content is created.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'trigger.test',
          ),
          'configure' => 'admin/structure/trigger',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'contextual' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/contextual/contextual.module',
        'basename' => 'contextual.module',
        'name' => 'contextual',
        'info' => 
        array (
          'name' => 'Contextual links',
          'description' => 'Provides contextual links to perform actions related to elements on a page.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'contextual.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'comment' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/comment/comment.module',
        'basename' => 'comment.module',
        'name' => 'comment',
        'info' => 
        array (
          'name' => 'Comment',
          'description' => 'Allows users to comment on and discuss published content.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'text',
          ),
          'files' => 
          array (
            0 => 'comment.module',
            1 => 'comment.test',
          ),
          'configure' => 'admin/content/comment',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'comment.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7009',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'toolbar' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/toolbar/toolbar.module',
        'basename' => 'toolbar.module',
        'name' => 'toolbar',
        'info' => 
        array (
          'name' => 'Toolbar',
          'description' => 'Provides a toolbar that shows the top-level administration menu items and links from other modules.',
          'core' => '7.x',
          'package' => 'Core',
          'version' => '7.21',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'dblog' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/dblog/dblog.module',
        'basename' => 'dblog.module',
        'name' => 'dblog',
        'info' => 
        array (
          'name' => 'Database logging',
          'description' => 'Logs and records system events to the database.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'dblog.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'contact' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/contact/contact.module',
        'basename' => 'contact.module',
        'name' => 'contact',
        'info' => 
        array (
          'name' => 'Contact',
          'description' => 'Enables the use of both personal and site-wide contact forms.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'contact.test',
          ),
          'configure' => 'admin/structure/contact',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'php' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/php/php.module',
        'basename' => 'php.module',
        'name' => 'php',
        'info' => 
        array (
          'name' => 'PHP filter',
          'description' => 'Allows embedded PHP code/snippets to be evaluated.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'php.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'color' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/color/color.module',
        'basename' => 'color.module',
        'name' => 'color',
        'info' => 
        array (
          'name' => 'Color',
          'description' => 'Allows administrators to change the color scheme of compatible themes.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'color.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'rdf_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/rdf/tests/rdf_test.module',
        'basename' => 'rdf_test.module',
        'name' => 'rdf_test',
        'info' => 
        array (
          'name' => 'RDF module tests',
          'description' => 'Support module for RDF module testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'rdf' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/rdf/rdf.module',
        'basename' => 'rdf.module',
        'name' => 'rdf',
        'info' => 
        array (
          'name' => 'RDF',
          'description' => 'Enriches your content with metadata to let other applications (e.g. search engines, aggregators) better understand its relationships and attributes.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rdf.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'forum' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/forum/forum.module',
        'basename' => 'forum.module',
        'name' => 'forum',
        'info' => 
        array (
          'name' => 'Forum',
          'description' => 'Provides discussion forums.',
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'comment',
          ),
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'forum.test',
          ),
          'configure' => 'admin/structure/forum',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'forum.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7012',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'file_module_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/file/tests/file_module_test.module',
        'basename' => 'file_module_test.module',
        'name' => 'file_module_test',
        'info' => 
        array (
          'name' => 'File test',
          'description' => 'Provides hooks for testing File module functionality.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'file' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/file/file.module',
        'basename' => 'file.module',
        'name' => 'file',
        'info' => 
        array (
          'name' => 'File',
          'description' => 'Defines a file field type.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'tests/file.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'drupal_system_listing_incompatible_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/drupal_system_listing_incompatible_test/drupal_system_listing_incompatible_test.module',
        'basename' => 'drupal_system_listing_incompatible_test.module',
        'name' => 'drupal_system_listing_incompatible_test',
        'info' => 
        array (
          'name' => 'Drupal system listing incompatible test',
          'description' => 'Support module for testing the drupal_system_listing function.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'drupal_system_listing_compatible_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/drupal_system_listing_compatible_test/drupal_system_listing_compatible_test.module',
        'basename' => 'drupal_system_listing_compatible_test.module',
        'name' => 'drupal_system_listing_compatible_test',
        'info' => 
        array (
          'name' => 'Drupal system listing compatible test',
          'description' => 'Support module for testing the drupal_system_listing function.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'entity_cache_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/entity_cache_test.module',
        'basename' => 'entity_cache_test.module',
        'name' => 'entity_cache_test',
        'info' => 
        array (
          'name' => 'Entity cache test',
          'description' => 'Support module for testing entity cache.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'entity_cache_test_dependency',
          ),
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'entity_query_access_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/entity_query_access_test.module',
        'basename' => 'entity_query_access_test.module',
        'name' => 'entity_query_access_test',
        'info' => 
        array (
          'name' => 'Entity query access test',
          'description' => 'Support module for checking entity query results.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'file_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/file_test.module',
        'basename' => 'file_test.module',
        'name' => 'file_test',
        'info' => 
        array (
          'name' => 'File test',
          'description' => 'Support module for file handling tests.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'file_test.module',
          ),
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'system_dependencies_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/system_dependencies_test.module',
        'basename' => 'system_dependencies_test.module',
        'name' => 'system_dependencies_test',
        'info' => 
        array (
          'name' => 'System dependency test',
          'description' => 'Support module for testing system dependencies.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'dependencies' => 
          array (
            0 => '_missing_dependency',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'system_incompatible_core_version_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/system_incompatible_core_version_test.module',
        'basename' => 'system_incompatible_core_version_test.module',
        'name' => 'system_incompatible_core_version_test',
        'info' => 
        array (
          'name' => 'System incompatible core version test',
          'description' => 'Support module for testing system dependencies.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '5.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'entity_crud_hook_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/entity_crud_hook_test.module',
        'basename' => 'entity_crud_hook_test.module',
        'name' => 'entity_crud_hook_test',
        'info' => 
        array (
          'name' => 'Entity CRUD Hooks Test',
          'description' => 'Support module for CRUD hook tests.',
          'core' => '7.x',
          'package' => 'Testing',
          'version' => '7.21',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'requirements2_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/requirements2_test.module',
        'basename' => 'requirements2_test.module',
        'name' => 'requirements2_test',
        'info' => 
        array (
          'name' => 'Requirements 2 Test',
          'description' => 'Tests that a module is not installed when the one it depends on fails hook_requirements(\'install).',
          'dependencies' => 
          array (
            0 => 'requirements1_test',
            1 => 'comment',
          ),
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'database_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/database_test.module',
        'basename' => 'database_test.module',
        'name' => 'database_test',
        'info' => 
        array (
          'name' => 'Database Test',
          'description' => 'Support module for Database layer tests.',
          'core' => '7.x',
          'package' => 'Testing',
          'version' => '7.21',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'system_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/system_test.module',
        'basename' => 'system_test.module',
        'name' => 'system_test',
        'info' => 
        array (
          'name' => 'System test',
          'description' => 'Support module for system testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'system_test.module',
          ),
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'entity_cache_test_dependency' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/entity_cache_test_dependency.module',
        'basename' => 'entity_cache_test_dependency.module',
        'name' => 'entity_cache_test_dependency',
        'info' => 
        array (
          'name' => 'Entity cache test dependency',
          'description' => 'Support dependency module for testing entity cache.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'form_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/form_test.module',
        'basename' => 'form_test.module',
        'name' => 'form_test',
        'info' => 
        array (
          'name' => 'FormAPI Test',
          'description' => 'Support module for Form API tests.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'batch_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/batch_test.module',
        'basename' => 'batch_test.module',
        'name' => 'batch_test',
        'info' => 
        array (
          'name' => 'Batch API test',
          'description' => 'Support module for Batch API tests.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'update_script_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/update_script_test.module',
        'basename' => 'update_script_test.module',
        'name' => 'update_script_test',
        'info' => 
        array (
          'name' => 'Update script test',
          'description' => 'Support module for update script testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'update_test_1' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/update_test_1.module',
        'basename' => 'update_test_1.module',
        'name' => 'update_test_1',
        'info' => 
        array (
          'name' => 'Update test',
          'description' => 'Support module for update testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'module_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/module_test.module',
        'basename' => 'module_test.module',
        'name' => 'module_test',
        'info' => 
        array (
          'name' => 'Module test',
          'description' => 'Support module for module system testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'filter_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/filter_test.module',
        'basename' => 'filter_test.module',
        'name' => 'filter_test',
        'info' => 
        array (
          'name' => 'Filter test module',
          'description' => 'Tests filter hooks and functions.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'theme_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/theme_test.module',
        'basename' => 'theme_test.module',
        'name' => 'theme_test',
        'info' => 
        array (
          'name' => 'Theme test',
          'description' => 'Support module for theme system testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'system_incompatible_module_version_dependencies_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/system_incompatible_module_version_dependencies_test.module',
        'basename' => 'system_incompatible_module_version_dependencies_test.module',
        'name' => 'system_incompatible_module_version_dependencies_test',
        'info' => 
        array (
          'name' => 'System incompatible module version dependencies test',
          'description' => 'Support module for testing system dependencies.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'dependencies' => 
          array (
            0 => 'system_incompatible_module_version_test (>2.0)',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'url_alter_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/url_alter_test.module',
        'basename' => 'url_alter_test.module',
        'name' => 'url_alter_test',
        'info' => 
        array (
          'name' => 'Url_alter tests',
          'description' => 'A support modules for url_alter hook testing.',
          'core' => '7.x',
          'package' => 'Testing',
          'version' => '7.21',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'menu_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/menu_test.module',
        'basename' => 'menu_test.module',
        'name' => 'menu_test',
        'info' => 
        array (
          'name' => 'Hook menu tests',
          'description' => 'Support module for menu hook testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'taxonomy_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/taxonomy_test.module',
        'basename' => 'taxonomy_test.module',
        'name' => 'taxonomy_test',
        'info' => 
        array (
          'name' => 'Taxonomy test module',
          'description' => '"Tests functions and hooks not used in core".',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'dependencies' => 
          array (
            0 => 'taxonomy',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'image_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/image_test.module',
        'basename' => 'image_test.module',
        'name' => 'image_test',
        'info' => 
        array (
          'name' => 'Image test',
          'description' => 'Support module for image toolkit tests.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'requirements1_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/requirements1_test.module',
        'basename' => 'requirements1_test.module',
        'name' => 'requirements1_test',
        'info' => 
        array (
          'name' => 'Requirements 1 Test',
          'description' => 'Tests that a module is not installed when it fails hook_requirements(\'install\').',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'system_incompatible_core_version_dependencies_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/system_incompatible_core_version_dependencies_test.module',
        'basename' => 'system_incompatible_core_version_dependencies_test.module',
        'name' => 'system_incompatible_core_version_dependencies_test',
        'info' => 
        array (
          'name' => 'System incompatible core version dependencies test',
          'description' => 'Support module for testing system dependencies.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'dependencies' => 
          array (
            0 => 'system_incompatible_core_version_test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'update_test_3' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/update_test_3.module',
        'basename' => 'update_test_3.module',
        'name' => 'update_test_3',
        'info' => 
        array (
          'name' => 'Update test',
          'description' => 'Support module for update testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'path_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/path_test.module',
        'basename' => 'path_test.module',
        'name' => 'path_test',
        'info' => 
        array (
          'name' => 'Hook path tests',
          'description' => 'Support module for path hook testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'error_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/error_test.module',
        'basename' => 'error_test.module',
        'name' => 'error_test',
        'info' => 
        array (
          'name' => 'Error test',
          'description' => 'Support module for error and exception testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'update_test_2' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/update_test_2.module',
        'basename' => 'update_test_2.module',
        'name' => 'update_test_2',
        'info' => 
        array (
          'name' => 'Update test',
          'description' => 'Support module for update testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'ajax_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/ajax_test.module',
        'basename' => 'ajax_test.module',
        'name' => 'ajax_test',
        'info' => 
        array (
          'name' => 'AJAX Test',
          'description' => 'Support module for AJAX framework tests.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'system_incompatible_module_version_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/system_incompatible_module_version_test.module',
        'basename' => 'system_incompatible_module_version_test.module',
        'name' => 'system_incompatible_module_version_test',
        'info' => 
        array (
          'name' => 'System incompatible module version test',
          'description' => 'Support module for testing system dependencies.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'session_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/session_test.module',
        'basename' => 'session_test.module',
        'name' => 'session_test',
        'info' => 
        array (
          'name' => 'Session test',
          'description' => 'Support module for session data testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'actions_loop_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/actions_loop_test.module',
        'basename' => 'actions_loop_test.module',
        'name' => 'actions_loop_test',
        'info' => 
        array (
          'name' => 'Actions loop test',
          'description' => 'Support module for action loop testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'ajax_forms_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/ajax_forms_test.module',
        'basename' => 'ajax_forms_test.module',
        'name' => 'ajax_forms_test',
        'info' => 
        array (
          'name' => 'AJAX form test mock module',
          'description' => 'Test for AJAX form calls.',
          'core' => '7.x',
          'package' => 'Testing',
          'version' => '7.21',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'common_test_cron_helper' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/common_test_cron_helper.module',
        'basename' => 'common_test_cron_helper.module',
        'name' => 'common_test_cron_helper',
        'info' => 
        array (
          'name' => 'Common Test Cron Helper',
          'description' => 'Helper module for CronRunTestCase::testCronExceptions().',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'common_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/common_test.module',
        'basename' => 'common_test.module',
        'name' => 'common_test',
        'info' => 
        array (
          'name' => 'Common Test',
          'description' => 'Support module for Common tests.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'common_test.css',
            ),
            'print' => 
            array (
              0 => 'common_test.print.css',
            ),
          ),
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'xmlrpc_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/tests/xmlrpc_test.module',
        'basename' => 'xmlrpc_test.module',
        'name' => 'xmlrpc_test',
        'info' => 
        array (
          'name' => 'XML-RPC Test',
          'description' => 'Support module for XML-RPC tests according to the validator1 specification.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'simpletest' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/simpletest/simpletest.module',
        'basename' => 'simpletest.module',
        'name' => 'simpletest',
        'info' => 
        array (
          'name' => 'Testing',
          'description' => 'Provides a framework for unit and functional testing.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'simpletest.test',
            1 => 'drupal_web_test_case.php',
            2 => 'tests/actions.test',
            3 => 'tests/ajax.test',
            4 => 'tests/batch.test',
            5 => 'tests/bootstrap.test',
            6 => 'tests/cache.test',
            7 => 'tests/common.test',
            8 => 'tests/database_test.test',
            9 => 'tests/entity_crud_hook_test.test',
            10 => 'tests/entity_query.test',
            11 => 'tests/error.test',
            12 => 'tests/file.test',
            13 => 'tests/filetransfer.test',
            14 => 'tests/form.test',
            15 => 'tests/graph.test',
            16 => 'tests/image.test',
            17 => 'tests/lock.test',
            18 => 'tests/mail.test',
            19 => 'tests/menu.test',
            20 => 'tests/module.test',
            21 => 'tests/pager.test',
            22 => 'tests/password.test',
            23 => 'tests/path.test',
            24 => 'tests/registry.test',
            25 => 'tests/schema.test',
            26 => 'tests/session.test',
            27 => 'tests/tablesort.test',
            28 => 'tests/theme.test',
            29 => 'tests/unicode.test',
            30 => 'tests/update.test',
            31 => 'tests/xmlrpc.test',
            32 => 'tests/upgrade/upgrade.test',
            33 => 'tests/upgrade/upgrade.comment.test',
            34 => 'tests/upgrade/upgrade.filter.test',
            35 => 'tests/upgrade/upgrade.forum.test',
            36 => 'tests/upgrade/upgrade.locale.test',
            37 => 'tests/upgrade/upgrade.menu.test',
            38 => 'tests/upgrade/upgrade.node.test',
            39 => 'tests/upgrade/upgrade.taxonomy.test',
            40 => 'tests/upgrade/upgrade.trigger.test',
            41 => 'tests/upgrade/upgrade.translatable.test',
            42 => 'tests/upgrade/upgrade.upload.test',
            43 => 'tests/upgrade/upgrade.user.test',
            44 => 'tests/upgrade/update.aggregator.test',
            45 => 'tests/upgrade/update.trigger.test',
            46 => 'tests/upgrade/update.field.test',
            47 => 'tests/upgrade/update.user.test',
          ),
          'configure' => 'admin/config/development/testing/settings',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'dashboard' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/dashboard/dashboard.module',
        'basename' => 'dashboard.module',
        'name' => 'dashboard',
        'info' => 
        array (
          'name' => 'Dashboard',
          'description' => 'Provides a dashboard page in the administrative interface for organizing administrative tasks and tracking information within your site.',
          'core' => '7.x',
          'package' => 'Core',
          'version' => '7.21',
          'files' => 
          array (
            0 => 'dashboard.test',
          ),
          'dependencies' => 
          array (
            0 => 'block',
          ),
          'configure' => 'admin/dashboard/customize',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'image_module_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/image/tests/image_module_test.module',
        'basename' => 'image_module_test.module',
        'name' => 'image_module_test',
        'info' => 
        array (
          'name' => 'Image test',
          'description' => 'Provides hook implementations for testing Image module functionality.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'image_module_test.module',
          ),
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'image' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/image/image.module',
        'basename' => 'image.module',
        'name' => 'image',
        'info' => 
        array (
          'name' => 'Image',
          'description' => 'Provides image manipulation tools.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'file',
          ),
          'files' => 
          array (
            0 => 'image.test',
          ),
          'configure' => 'admin/config/media/image-styles',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'syslog' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/syslog/syslog.module',
        'basename' => 'syslog.module',
        'name' => 'syslog',
        'info' => 
        array (
          'name' => 'Syslog',
          'description' => 'Logs and records system events to syslog.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'syslog.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'user_form_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/user/tests/user_form_test.module',
        'basename' => 'user_form_test.module',
        'name' => 'user_form_test',
        'info' => 
        array (
          'name' => 'User module form tests',
          'description' => 'Support module for user form testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'user' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/user/user.module',
        'basename' => 'user.module',
        'name' => 'user',
        'info' => 
        array (
          'name' => 'User',
          'description' => 'Manages the user registration and login system.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'user.module',
            1 => 'user.test',
          ),
          'required' => true,
          'configure' => 'admin/config/people',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'user.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7018',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'node_test_exception' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/node/tests/node_test_exception.module',
        'basename' => 'node_test_exception.module',
        'name' => 'node_test_exception',
        'info' => 
        array (
          'name' => 'Node module exception tests',
          'description' => 'Support module for node related exception testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'node_access_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/node/tests/node_access_test.module',
        'basename' => 'node_access_test.module',
        'name' => 'node_access_test',
        'info' => 
        array (
          'name' => 'Node module access tests',
          'description' => 'Support module for node permission testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'node_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/node/tests/node_test.module',
        'basename' => 'node_test.module',
        'name' => 'node_test',
        'info' => 
        array (
          'name' => 'Node module tests',
          'description' => 'Support module for node related testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'node' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/node/node.module',
        'basename' => 'node.module',
        'name' => 'node',
        'info' => 
        array (
          'name' => 'Node',
          'description' => 'Allows content to be submitted to the site and displayed on pages.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'node.module',
            1 => 'node.test',
          ),
          'required' => true,
          'configure' => 'admin/structure/types',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'node.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7013',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'search_embedded_form' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/search/tests/search_embedded_form.module',
        'basename' => 'search_embedded_form.module',
        'name' => 'search_embedded_form',
        'info' => 
        array (
          'name' => 'Search embedded form',
          'description' => 'Support module for search module testing of embedded forms.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'search_extra_type' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/search/tests/search_extra_type.module',
        'basename' => 'search_extra_type.module',
        'name' => 'search_extra_type',
        'info' => 
        array (
          'name' => 'Test search type',
          'description' => 'Support module for search module testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'search' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/search/search.module',
        'basename' => 'search.module',
        'name' => 'search',
        'info' => 
        array (
          'name' => 'Search',
          'description' => 'Enables site-wide keyword searching.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'search.extender.inc',
            1 => 'search.test',
          ),
          'configure' => 'admin/config/search/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'search.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'system' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/system/system.module',
        'basename' => 'system.module',
        'name' => 'system',
        'info' => 
        array (
          'name' => 'System',
          'description' => 'Handles general site configuration for administrators.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'system.archiver.inc',
            1 => 'system.mail.inc',
            2 => 'system.queue.inc',
            3 => 'system.tar.inc',
            4 => 'system.updater.inc',
            5 => 'system.test',
          ),
          'required' => true,
          'configure' => 'admin/config/system',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7077',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'help' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/help/help.module',
        'basename' => 'help.module',
        'name' => 'help',
        'info' => 
        array (
          'name' => 'Help',
          'description' => 'Manages the display of online help.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'help.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'taxonomy' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/taxonomy/taxonomy.module',
        'basename' => 'taxonomy.module',
        'name' => 'taxonomy',
        'info' => 
        array (
          'name' => 'Taxonomy',
          'description' => 'Enables the categorization of content.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'options',
          ),
          'files' => 
          array (
            0 => 'taxonomy.module',
            1 => 'taxonomy.test',
          ),
          'configure' => 'admin/structure/taxonomy',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7010',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'ccc_update_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/update/tests/ccc_update_test.module',
        'basename' => 'ccc_update_test.module',
        'name' => 'ccc_update_test',
        'info' => 
        array (
          'name' => 'CCC Update test',
          'description' => 'Support module for update module testing.',
          'package' => 'Testing',
          'core' => '7.x',
          'hidden' => true,
          'version' => '7.21',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'bbb_update_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/update/tests/bbb_update_test.module',
        'basename' => 'bbb_update_test.module',
        'name' => 'bbb_update_test',
        'info' => 
        array (
          'name' => 'BBB Update test',
          'description' => 'Support module for update module testing.',
          'package' => 'Testing',
          'core' => '7.x',
          'hidden' => true,
          'version' => '7.21',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'update_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/update/tests/update_test.module',
        'basename' => 'update_test.module',
        'name' => 'update_test',
        'info' => 
        array (
          'name' => 'Update test',
          'description' => 'Support module for update module testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'aaa_update_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/update/tests/aaa_update_test.module',
        'basename' => 'aaa_update_test.module',
        'name' => 'aaa_update_test',
        'info' => 
        array (
          'name' => 'AAA Update test',
          'description' => 'Support module for update module testing.',
          'package' => 'Testing',
          'core' => '7.x',
          'hidden' => true,
          'version' => '7.21',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'update' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/update/update.module',
        'basename' => 'update.module',
        'name' => 'update',
        'info' => 
        array (
          'name' => 'Update manager',
          'description' => 'Checks for available updates, and can securely install or update modules and themes via a web interface.',
          'version' => '7.21',
          'package' => 'Core',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'update.test',
          ),
          'configure' => 'admin/reports/updates/settings',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'poll' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/poll/poll.module',
        'basename' => 'poll.module',
        'name' => 'poll',
        'info' => 
        array (
          'name' => 'Poll',
          'description' => 'Allows your site to capture votes on different topics in the form of multiple choice questions.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'poll.test',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'poll.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'overlay' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/overlay/overlay.module',
        'basename' => 'overlay.module',
        'name' => 'overlay',
        'info' => 
        array (
          'name' => 'Overlay',
          'description' => 'Displays the Drupal administration interface in an overlay.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'shortcut' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/shortcut/shortcut.module',
        'basename' => 'shortcut.module',
        'name' => 'shortcut',
        'info' => 
        array (
          'name' => 'Shortcut',
          'description' => 'Allows users to manage customizable lists of shortcut links.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'shortcut.test',
          ),
          'configure' => 'admin/config/user-interface/shortcut',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'openid_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/openid/tests/openid_test.module',
        'basename' => 'openid_test.module',
        'name' => 'openid_test',
        'info' => 
        array (
          'name' => 'OpenID dummy provider',
          'description' => 'OpenID provider used for testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'openid',
          ),
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'openid' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/openid/openid.module',
        'basename' => 'openid.module',
        'name' => 'openid',
        'info' => 
        array (
          'name' => 'OpenID',
          'description' => 'Allows users to log into your site using OpenID.',
          'version' => '7.21',
          'package' => 'Core',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'openid.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6000',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'blog' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/blog/blog.module',
        'basename' => 'blog.module',
        'name' => 'blog',
        'info' => 
        array (
          'name' => 'Blog',
          'description' => 'Enables multi-user blogs.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'blog.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'filter' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/filter/filter.module',
        'basename' => 'filter.module',
        'name' => 'filter',
        'info' => 
        array (
          'name' => 'Filter',
          'description' => 'Filters content in preparation for display.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'filter.test',
          ),
          'required' => true,
          'configure' => 'admin/config/content/formats',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7010',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'aggregator_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/aggregator/tests/aggregator_test.module',
        'basename' => 'aggregator_test.module',
        'name' => 'aggregator_test',
        'info' => 
        array (
          'name' => 'Aggregator module tests',
          'description' => 'Support module for aggregator related testing.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'aggregator' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/aggregator/aggregator.module',
        'basename' => 'aggregator.module',
        'name' => 'aggregator',
        'info' => 
        array (
          'name' => 'Aggregator',
          'description' => 'Aggregates syndicated content (RSS, RDF, and Atom feeds).',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'aggregator.test',
          ),
          'configure' => 'admin/config/services/aggregator/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'aggregator.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'menu' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/menu/menu.module',
        'basename' => 'menu.module',
        'name' => 'menu',
        'info' => 
        array (
          'name' => 'Menu',
          'description' => 'Allows administrators to customize the site navigation menu.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'menu.test',
          ),
          'configure' => 'admin/structure/menu',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'block_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/block/tests/block_test.module',
        'basename' => 'block_test.module',
        'name' => 'block_test',
        'info' => 
        array (
          'name' => 'Block test',
          'description' => 'Provides test blocks.',
          'package' => 'Testing',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'block' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/block/block.module',
        'basename' => 'block.module',
        'name' => 'block',
        'info' => 
        array (
          'name' => 'Block',
          'description' => 'Controls the visual building blocks a page is constructed with. Blocks are boxes of content rendered into an area, or region, of a web page.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'block.test',
          ),
          'configure' => 'admin/structure/block',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7008',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'book' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/book/book.module',
        'basename' => 'book.module',
        'name' => 'book',
        'info' => 
        array (
          'name' => 'Book',
          'description' => 'Allows users to create and organize related content in an outline.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'book.test',
          ),
          'configure' => 'admin/content/book/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'book.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'list_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field/modules/list/tests/list_test.module',
        'basename' => 'list_test.module',
        'name' => 'list_test',
        'info' => 
        array (
          'name' => 'List test',
          'description' => 'Support module for the List module tests.',
          'core' => '7.x',
          'package' => 'Testing',
          'version' => '7.21',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'list' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field/modules/list/list.module',
        'basename' => 'list.module',
        'name' => 'list',
        'info' => 
        array (
          'name' => 'List',
          'description' => 'Defines list field types. Use with Options to create selection lists.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'options',
          ),
          'files' => 
          array (
            0 => 'tests/list.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'field_sql_storage' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field/modules/field_sql_storage/field_sql_storage.module',
        'basename' => 'field_sql_storage.module',
        'name' => 'field_sql_storage',
        'info' => 
        array (
          'name' => 'Field SQL storage',
          'description' => 'Stores field data in an SQL database.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'field_sql_storage.test',
          ),
          'required' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'text' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field/modules/text/text.module',
        'basename' => 'text.module',
        'name' => 'text',
        'info' => 
        array (
          'name' => 'Text',
          'description' => 'Defines simple text field types.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'text.test',
          ),
          'required' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'number' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field/modules/number/number.module',
        'basename' => 'number.module',
        'name' => 'number',
        'info' => 
        array (
          'name' => 'Number',
          'description' => 'Defines numeric field types.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'number.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'options' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field/modules/options/options.module',
        'basename' => 'options.module',
        'name' => 'options',
        'info' => 
        array (
          'name' => 'Options',
          'description' => 'Defines selection, check box and radio button widgets for text and numeric fields.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'options.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'field_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field/tests/field_test.module',
        'basename' => 'field_test.module',
        'name' => 'field_test',
        'info' => 
        array (
          'name' => 'Field API Test',
          'description' => 'Support module for the Field API tests.',
          'core' => '7.x',
          'package' => 'Testing',
          'files' => 
          array (
            0 => 'field_test.entity.inc',
          ),
          'version' => '7.21',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'field' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field/field.module',
        'basename' => 'field.module',
        'name' => 'field',
        'info' => 
        array (
          'name' => 'Field',
          'description' => 'Field API to add fields to entities like nodes and users.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field.module',
            1 => 'field.attach.inc',
            2 => 'tests/field.test',
          ),
          'dependencies' => 
          array (
            0 => 'field_sql_storage',
          ),
          'required' => true,
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'theme/field.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'path' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/path/path.module',
        'basename' => 'path.module',
        'name' => 'path',
        'info' => 
        array (
          'name' => 'Path',
          'description' => 'Allows users to rename URLs.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'path.test',
          ),
          'configure' => 'admin/config/search/path',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'translation_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/translation/tests/translation_test.module',
        'basename' => 'translation_test.module',
        'name' => 'translation_test',
        'info' => 
        array (
          'name' => 'Content Translation Test',
          'description' => 'Support module for the content translation tests.',
          'core' => '7.x',
          'package' => 'Testing',
          'version' => '7.21',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'translation' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/translation/translation.module',
        'basename' => 'translation.module',
        'name' => 'translation',
        'info' => 
        array (
          'name' => 'Content translation',
          'description' => 'Allows content to be translated into different languages.',
          'dependencies' => 
          array (
            0 => 'locale',
          ),
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'translation.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'tracker' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/tracker/tracker.module',
        'basename' => 'tracker.module',
        'name' => 'tracker',
        'info' => 
        array (
          'name' => 'Tracker',
          'description' => 'Enables tracking of recent content for users.',
          'dependencies' => 
          array (
            0 => 'comment',
          ),
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tracker.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'profile' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/profile/profile.module',
        'basename' => 'profile.module',
        'name' => 'profile',
        'info' => 
        array (
          'name' => 'Profile',
          'description' => 'Supports configurable user profiles.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'profile.test',
          ),
          'configure' => 'admin/config/people/profile',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'field_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/field_ui/field_ui.module',
        'basename' => 'field_ui.module',
        'name' => 'field_ui',
        'info' => 
        array (
          'name' => 'Field UI',
          'description' => 'User interface for the Field API.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'field_ui.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'statistics' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/statistics/statistics.module',
        'basename' => 'statistics.module',
        'name' => 'statistics',
        'info' => 
        array (
          'name' => 'Statistics',
          'description' => 'Logs access statistics for your site.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'statistics.test',
          ),
          'configure' => 'admin/config/system/statistics',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'locale_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/locale/tests/locale_test.module',
        'basename' => 'locale_test.module',
        'name' => 'locale_test',
        'info' => 
        array (
          'name' => 'Locale Test',
          'description' => 'Support module for the locale layer tests.',
          'core' => '7.x',
          'package' => 'Testing',
          'version' => '7.21',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'locale' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/modules/locale/locale.module',
        'basename' => 'locale.module',
        'name' => 'locale',
        'info' => 
        array (
          'name' => 'Locale',
          'description' => 'Adds language handling functionality and enables the translation of the user interface to languages other than English.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'locale.test',
          ),
          'configure' => 'admin/config/regional/language',
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'project' => 'drupal',
        'version' => '7.21',
      ),
    ),
    'themes' => 
    array (
      'seven' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/themes/seven/seven.info',
        'basename' => 'seven.info',
        'name' => 'Seven',
        'info' => 
        array (
          'name' => 'Seven',
          'description' => 'A simple one-column, tableless, fluid width administration theme.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'screen' => 
            array (
              0 => 'reset.css',
              1 => 'style.css',
            ),
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '1',
          ),
          'regions' => 
          array (
            'content' => 'Content',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'sidebar_first' => 'First sidebar',
          ),
          'regions_hidden' => 
          array (
            0 => 'sidebar_first',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
        ),
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'bartik' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/themes/bartik/bartik.info',
        'basename' => 'bartik.info',
        'name' => 'Bartik',
        'info' => 
        array (
          'name' => 'Bartik',
          'description' => 'A flexible, recolorable theme with many regions.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/layout.css',
              1 => 'css/style.css',
              2 => 'css/colors.css',
            ),
            'print' => 
            array (
              0 => 'css/print.css',
            ),
          ),
          'regions' => 
          array (
            'header' => 'Header',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'highlighted' => 'Highlighted',
            'featured' => 'Featured',
            'content' => 'Content',
            'sidebar_first' => 'Sidebar first',
            'sidebar_second' => 'Sidebar second',
            'triptych_first' => 'Triptych first',
            'triptych_middle' => 'Triptych middle',
            'triptych_last' => 'Triptych last',
            'footer_firstcolumn' => 'Footer first column',
            'footer_secondcolumn' => 'Footer second column',
            'footer_thirdcolumn' => 'Footer third column',
            'footer_fourthcolumn' => 'Footer fourth column',
            'footer' => 'Footer',
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '0',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
        ),
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'stark' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/themes/stark/stark.info',
        'basename' => 'stark.info',
        'name' => 'Stark',
        'info' => 
        array (
          'name' => 'Stark',
          'description' => 'This theme demonstrates Drupal\'s default HTML markup and CSS styles. To learn how to build your own theme and override Drupal\'s default code, see the <a href="http://drupal.org/theme-guide">Theming Guide</a>.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'layout.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
        ),
        'project' => 'drupal',
        'version' => '7.21',
      ),
      'garland' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/themes/garland/garland.info',
        'basename' => 'garland.info',
        'name' => 'Garland',
        'info' => 
        array (
          'name' => 'Garland',
          'description' => 'A multi-column theme which can be configured to modify colors and switch between fixed and fluid width layouts.',
          'package' => 'Core',
          'version' => '7.21',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'style.css',
            ),
            'print' => 
            array (
              0 => 'print.css',
            ),
          ),
          'settings' => 
          array (
            'garland_width' => 'fluid',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
        ),
        'project' => 'drupal',
        'version' => '7.21',
      ),
    ),
    'platforms' => 
    array (
      'drupal' => 
      array (
        'short_name' => 'drupal',
        'version' => '7.21',
        'description' => 'This platform is running Drupal 7.21',
      ),
    ),
    'profiles' => 
    array (
      'minimal' => 
      array (
        'name' => 'minimal',
        'filename' => '/var/aegir/platforms/gdp.dev1.a/profiles/minimal/minimal.profile',
        'project' => 'drupal',
        'info' => 
        array (
          'name' => 'Minimal',
          'description' => 'Start with only a few modules enabled.',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'dblog',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
        ),
        'version' => '7.21',
      ),
      'standard' => 
      array (
        'name' => 'standard',
        'filename' => '/var/aegir/platforms/gdp.dev1.a/profiles/standard/standard.profile',
        'project' => 'drupal',
        'info' => 
        array (
          'name' => 'Standard',
          'description' => 'Install with commonly used features pre-configured.',
          'version' => '7.21',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'color',
            2 => 'comment',
            3 => 'contextual',
            4 => 'dashboard',
            5 => 'help',
            6 => 'image',
            7 => 'list',
            8 => 'menu',
            9 => 'number',
            10 => 'options',
            11 => 'path',
            12 => 'taxonomy',
            13 => 'dblog',
            14 => 'search',
            15 => 'shortcut',
            16 => 'toolbar',
            17 => 'overlay',
            18 => 'field_ui',
            19 => 'file',
            20 => 'rdf',
          ),
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
          'old_short_name' => 'default',
        ),
        'version' => '7.21',
      ),
      'testing' => 
      array (
        'name' => 'testing',
        'filename' => '/var/aegir/platforms/gdp.dev1.a/profiles/testing/testing.profile',
        'project' => 'drupal',
        'info' => 
        array (
          'name' => 'Testing',
          'description' => 'Minimal profile for running tests. Includes absolutely required modules only.',
          'version' => '7.21',
          'core' => '7.x',
          'hidden' => true,
          'project' => 'drupal',
          'datestamp' => '1362616996',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
        ),
        'version' => '7.21',
      ),
    ),
  ),
  'sites-all' => 
  array (
    'modules' => 
    array (
      'ds_forms' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ds/modules/ds_forms/ds_forms.module',
        'basename' => 'ds_forms.module',
        'name' => 'ds_forms',
        'info' => 
        array (
          'name' => 'Display Suite Forms',
          'description' => 'Manage the layout of forms in Display Suite.',
          'core' => '7.x',
          'package' => 'Display suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'version' => '7.x-1.7',
          'project' => 'ds',
          'datestamp' => '1361374867',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-1.7',
      ),
      'ds_search' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ds/modules/ds_search/ds_search.module',
        'basename' => 'ds_search.module',
        'name' => 'ds_search',
        'info' => 
        array (
          'name' => 'Display Suite Search',
          'description' => 'Extend the display options for search results for Drupal Core or Apache Solr.',
          'core' => '7.x',
          'package' => 'Display suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/search',
          'version' => '7.x-1.7',
          'project' => 'ds',
          'datestamp' => '1361374867',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-1.7',
      ),
      'ds_extras' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ds/modules/ds_extras/ds_extras.module',
        'basename' => 'ds_extras.module',
        'name' => 'ds_extras',
        'info' => 
        array (
          'name' => 'Display Suite Extras',
          'description' => 'Contains additional features for Display Suite.',
          'core' => '7.x',
          'package' => 'Display suite',
          'dependencies' => 
          array (
            0 => 'ds',
          ),
          'configure' => 'admin/structure/ds/extras',
          'version' => '7.x-1.7',
          'project' => 'ds',
          'datestamp' => '1361374867',
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'project' => 'ds',
        'version' => '7.x-1.7',
      ),
      'ds_exportables_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ds/tests/ds_exportables_test/ds_exportables_test.module',
        'basename' => 'ds_exportables_test.module',
        'name' => 'ds_exportables_test',
        'info' => 
        array (
          'name' => 'Display suite exportables test',
          'description' => 'Tests for exportables with Display Suite.',
          'package' => 'Display suite',
          'core' => '7.x',
          'hidden' => true,
          'version' => '7.x-1.7',
          'project' => 'ds',
          'datestamp' => '1361374867',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-1.7',
      ),
      'ds_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ds/tests/ds_test.module',
        'basename' => 'ds_test.module',
        'name' => 'ds_test',
        'info' => 
        array (
          'name' => 'Display suite Test',
          'description' => 'Test module for display suite',
          'core' => '7.x',
          'package' => 'Display suite',
          'dependencies' => 
          array (
            0 => 'ds_extras',
          ),
          'hidden' => true,
          'version' => '7.x-1.7',
          'project' => 'ds',
          'datestamp' => '1361374867',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ds',
        'version' => '7.x-1.7',
      ),
      'ds' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ds/ds.module',
        'basename' => 'ds.module',
        'name' => 'ds',
        'info' => 
        array (
          'name' => 'Display suite',
          'description' => 'Extend the display options for every entity type.',
          'core' => '7.x',
          'package' => 'Display suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'views/views_plugin_ds_entity_view.inc',
            1 => 'views/views_plugin_ds_fields_view.inc',
            2 => 'tests/ds.base.test',
            3 => 'tests/ds.search.test',
            4 => 'tests/ds.entities.test',
            5 => 'tests/ds.exportables.test',
            6 => 'tests/ds.panels.test',
            7 => 'tests/ds.views.test',
            8 => 'tests/ds.forms.test',
          ),
          'configure' => 'admin/structure/ds',
          'version' => '7.x-1.7',
          'project' => 'ds',
          'datestamp' => '1361374867',
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'project' => 'ds',
        'version' => '7.x-1.7',
      ),
      'strongarm' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/strongarm/strongarm.module',
        'basename' => 'strongarm.module',
        'name' => 'strongarm',
        'info' => 
        array (
          'name' => 'Strongarm',
          'description' => 'Enforces variable values defined by modules that need settings set to operate properly.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'strongarm.admin.inc',
            1 => 'strongarm.install',
            2 => 'strongarm.module',
          ),
          'version' => '7.x-2.0',
          'project' => 'strongarm',
          'datestamp' => '1339604214',
          'php' => '5.2.4',
        ),
        'schema_version' => '7201',
        'project' => 'strongarm',
        'version' => '7.x-2.0',
      ),
      'argfilters' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/views_arguments_in_filters/argfilters.module',
        'basename' => 'argfilters.module',
        'name' => 'argfilters',
        'info' => 
        array (
          'name' => 'Views arguments in filters',
          'description' => 'Allows you to use argument values in filters, thereby leveraging functionality such as operators.',
          'package' => 'Experimental',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'entity_feature' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/entity/tests/entity_feature.module',
        'basename' => 'entity_feature.module',
        'name' => 'entity_feature',
        'info' => 
        array (
          'name' => 'Entity feature module',
          'description' => 'Provides some entities in code.',
          'version' => '7.x-1.0',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity_feature.module',
          ),
          'dependencies' => 
          array (
            0 => 'entity_test',
          ),
          'hidden' => true,
          'project' => 'entity',
          'datestamp' => '1356471145',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entity',
        'version' => '7.x-1.0',
      ),
      'entity_test_i18n' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/entity/tests/entity_test_i18n.module',
        'basename' => 'entity_test_i18n.module',
        'name' => 'entity_test_i18n',
        'info' => 
        array (
          'name' => 'Entity-test type translation',
          'description' => 'Allows translating entity-test types.',
          'dependencies' => 
          array (
            0 => 'entity_test',
            1 => 'i18n_string',
          ),
          'package' => 'Multilingual - Internationalization',
          'core' => '7.x',
          'hidden' => true,
          'version' => '7.x-1.0',
          'project' => 'entity',
          'datestamp' => '1356471145',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entity',
        'version' => '7.x-1.0',
      ),
      'entity_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/entity/tests/entity_test.module',
        'basename' => 'entity_test.module',
        'name' => 'entity_test',
        'info' => 
        array (
          'name' => 'Entity CRUD test module',
          'description' => 'Provides entity types based upon the CRUD API.',
          'version' => '7.x-1.0',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity_test.module',
            1 => 'entity_test.install',
          ),
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'hidden' => true,
          'project' => 'entity',
          'datestamp' => '1356471145',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entity',
        'version' => '7.x-1.0',
      ),
      'entity' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/entity/entity.module',
        'basename' => 'entity.module',
        'name' => 'entity',
        'info' => 
        array (
          'name' => 'Entity API',
          'description' => 'Enables modules to work with any entity type and to provide entities.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity.features.inc',
            1 => 'entity.i18n.inc',
            2 => 'entity.info.inc',
            3 => 'entity.rules.inc',
            4 => 'entity.test',
            5 => 'includes/entity.inc',
            6 => 'includes/entity.controller.inc',
            7 => 'includes/entity.ui.inc',
            8 => 'includes/entity.wrapper.inc',
            9 => 'views/entity.views.inc',
            10 => 'views/handlers/entity_views_field_handler_helper.inc',
            11 => 'views/handlers/entity_views_handler_area_entity.inc',
            12 => 'views/handlers/entity_views_handler_field_boolean.inc',
            13 => 'views/handlers/entity_views_handler_field_date.inc',
            14 => 'views/handlers/entity_views_handler_field_duration.inc',
            15 => 'views/handlers/entity_views_handler_field_entity.inc',
            16 => 'views/handlers/entity_views_handler_field_field.inc',
            17 => 'views/handlers/entity_views_handler_field_numeric.inc',
            18 => 'views/handlers/entity_views_handler_field_options.inc',
            19 => 'views/handlers/entity_views_handler_field_text.inc',
            20 => 'views/handlers/entity_views_handler_field_uri.inc',
            21 => 'views/handlers/entity_views_handler_relationship_by_bundle.inc',
            22 => 'views/handlers/entity_views_handler_relationship.inc',
            23 => 'views/plugins/entity_views_plugin_row_entity_view.inc',
          ),
          'version' => '7.x-1.0',
          'project' => 'entity',
          'datestamp' => '1356471145',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'entity',
        'version' => '7.x-1.0',
      ),
      'entity_token' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/entity/entity_token.module',
        'basename' => 'entity_token.module',
        'name' => 'entity_token',
        'info' => 
        array (
          'name' => 'Entity tokens',
          'description' => 'Provides token replacements for all properties that have no tokens and are known to the entity API.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'entity_token.tokens.inc',
            1 => 'entity_token.module',
          ),
          'dependencies' => 
          array (
            0 => 'entity',
          ),
          'version' => '7.x-1.0',
          'project' => 'entity',
          'datestamp' => '1356471145',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entity',
        'version' => '7.x-1.0',
      ),
      'ctools_ajax_sample' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/ctools_ajax_sample/ctools_ajax_sample.module',
        'basename' => 'ctools_ajax_sample.module',
        'name' => 'ctools_ajax_sample',
        'info' => 
        array (
          'name' => 'Chaos Tools (CTools) AJAX Example',
          'description' => 'Shows how to use the power of Chaos AJAX.',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'ctools_access_ruleset' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/ctools_access_ruleset/ctools_access_ruleset.module',
        'basename' => 'ctools_access_ruleset.module',
        'name' => 'ctools_access_ruleset',
        'info' => 
        array (
          'name' => 'Custom rulesets',
          'description' => 'Create custom, exportable, reusable access rulesets for applications like Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'bulk_export' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/bulk_export/bulk_export.module',
        'basename' => 'bulk_export.module',
        'name' => 'bulk_export',
        'info' => 
        array (
          'name' => 'Bulk Export',
          'description' => 'Performs bulk exporting of data objects known about by Chaos tools.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'ctools_export_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/tests/ctools_export_test/ctools_export_test.module',
        'basename' => 'ctools_export_test.module',
        'name' => 'ctools_export_test',
        'info' => 
        array (
          'name' => 'CTools export test',
          'description' => 'CTools export test module',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'hidden' => true,
          'files' => 
          array (
            0 => 'ctools_export.test',
          ),
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'ctools_plugin_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/tests/ctools_plugin_test.module',
        'basename' => 'ctools_plugin_test.module',
        'name' => 'ctools_plugin_test',
        'info' => 
        array (
          'name' => 'Chaos tools plugins test',
          'description' => 'Provides hooks for testing ctools plugins.',
          'package' => 'Chaos tool suite',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'ctools.plugins.test',
            1 => 'object_cache.test',
            2 => 'css.test',
            3 => 'context.test',
          ),
          'hidden' => true,
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'page_manager' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/page_manager/page_manager.module',
        'basename' => 'page_manager.module',
        'name' => 'page_manager',
        'info' => 
        array (
          'name' => 'Page manager',
          'description' => 'Provides a UI and API to manage pages within the site.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'package' => 'Chaos tool suite',
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'ctools_plugin_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/ctools_plugin_example/ctools_plugin_example.module',
        'basename' => 'ctools_plugin_example.module',
        'name' => 'ctools_plugin_example',
        'info' => 
        array (
          'name' => 'Chaos Tools (CTools) Plugin Example',
          'description' => 'Shows how an external module can provide ctools plugins (for Panels, etc.).',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'panels',
            2 => 'page_manager',
            3 => 'advanced_help',
          ),
          'core' => '7.x',
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'views_content' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/views_content/views_content.module',
        'basename' => 'views_content.module',
        'name' => 'views_content',
        'info' => 
        array (
          'name' => 'Views content panes',
          'description' => 'Allows Views content to be used in Panels, Dashboard and other modules which use the CTools Content API.',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'views',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'plugins/views/views_content_plugin_display_ctools_context.inc',
            1 => 'plugins/views/views_content_plugin_display_panel_pane.inc',
            2 => 'plugins/views/views_content_plugin_style_ctools_context.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'ctools_custom_content' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/ctools_custom_content/ctools_custom_content.module',
        'basename' => 'ctools_custom_content.module',
        'name' => 'ctools_custom_content',
        'info' => 
        array (
          'name' => 'Custom content panes',
          'description' => 'Create custom, exportable, reusable content panes for applications like Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'stylizer' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/stylizer/stylizer.module',
        'basename' => 'stylizer.module',
        'name' => 'stylizer',
        'info' => 
        array (
          'name' => 'Stylizer',
          'description' => 'Create custom styles for applications such as Panels.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'dependencies' => 
          array (
            0 => 'ctools',
            1 => 'color',
          ),
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'ctools' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/ctools/ctools.module',
        'basename' => 'ctools.module',
        'name' => 'ctools',
        'info' => 
        array (
          'name' => 'Chaos tools',
          'description' => 'A library of helpful tools by Merlin of Chaos.',
          'core' => '7.x',
          'package' => 'Chaos tool suite',
          'files' => 
          array (
            0 => 'includes/context.inc',
            1 => 'includes/math-expr.inc',
            2 => 'includes/stylizer.inc',
          ),
          'version' => '7.x-1.2',
          'project' => 'ctools',
          'datestamp' => '1345319204',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6007',
        'project' => 'ctools',
        'version' => '7.x-1.2',
      ),
      'entityreference_behavior_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/entityreference/examples/entityreference_behavior_example/entityreference_behavior_example.module',
        'basename' => 'entityreference_behavior_example.module',
        'name' => 'entityreference_behavior_example',
        'info' => 
        array (
          'name' => 'Entity Reference Behavior Example',
          'description' => 'Provides some example code for implementing Entity Reference behaviors.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entityreference',
          ),
          'version' => '7.x-1.0',
          'project' => 'entityreference',
          'datestamp' => '1353230808',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'entityreference',
        'version' => '7.x-1.0',
      ),
      'entityreference' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/entityreference/entityreference.module',
        'basename' => 'entityreference.module',
        'name' => 'entityreference',
        'info' => 
        array (
          'name' => 'Entity Reference',
          'description' => 'Provides a field that can reference other entities.',
          'core' => '7.x',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'entity',
            1 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'entityreference.migrate.inc',
            1 => 'plugins/selection/abstract.inc',
            2 => 'plugins/selection/views.inc',
            3 => 'plugins/behavior/abstract.inc',
            4 => 'views/entityreference_plugin_display.inc',
            5 => 'views/entityreference_plugin_style.inc',
            6 => 'views/entityreference_plugin_row_fields.inc',
            7 => 'tests/entityreference.handlers.test',
            8 => 'tests/entityreference.taxonomy.test',
            9 => 'tests/entityreference.admin.test',
          ),
          'version' => '7.x-1.0',
          'project' => 'entityreference',
          'datestamp' => '1353230808',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'entityreference',
        'version' => '7.x-1.0',
      ),
      'field_group' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/field_group/field_group.module',
        'basename' => 'field_group.module',
        'name' => 'field_group',
        'info' => 
        array (
          'name' => 'Fieldgroup',
          'description' => 'Fieldgroup',
          'package' => 'Fields',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'ctools',
          ),
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field_group.install',
            1 => 'field_group.module',
            2 => 'field_group.field_ui.inc',
            3 => 'field_group.form.inc',
            4 => 'field_group.features.inc',
            5 => 'field_group.test',
          ),
          'version' => '7.x-1.1',
          'project' => 'field_group',
          'datestamp' => '1319051133',
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'project' => 'field_group',
        'version' => '7.x-1.1',
      ),
      'module_filter' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/module_filter/module_filter.module',
        'basename' => 'module_filter.module',
        'name' => 'module_filter',
        'info' => 
        array (
          'name' => 'Module filter',
          'description' => 'Filter the modules list.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'module_filter.install',
            1 => 'module_filter.js',
            2 => 'module_filter.module',
            3 => 'module_filter.admin.inc',
            4 => 'module_filter.theme.inc',
            5 => 'css/module_filter.css',
            6 => 'css/module_filter_tab.css',
            7 => 'js/module_filter.js',
            8 => 'js/module_filter_tab.js',
          ),
          'configure' => 'admin/config/user-interface/modulefilter',
          'version' => '7.x-1.7',
          'project' => 'module_filter',
          'datestamp' => '1341518501',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7100',
        'project' => 'module_filter',
        'version' => '7.x-1.7',
      ),
      'admin_devel' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/admin_menu/admin_devel/admin_devel.module',
        'basename' => 'admin_devel.module',
        'name' => 'admin_devel',
        'info' => 
        array (
          'name' => 'Administration Development tools',
          'description' => 'Administration and debugging functionality for developers and site builders.',
          'package' => 'Administration',
          'core' => '7.x',
          'scripts' => 
          array (
            0 => 'admin_devel.js',
          ),
          'version' => '7.x-3.0-rc4',
          'project' => 'admin_menu',
          'datestamp' => '1359651687',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'admin_menu',
        'version' => '7.x-3.0-rc4',
      ),
      'admin_menu_toolbar' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/admin_menu/admin_menu_toolbar/admin_menu_toolbar.module',
        'basename' => 'admin_menu_toolbar.module',
        'name' => 'admin_menu_toolbar',
        'info' => 
        array (
          'name' => 'Administration menu Toolbar style',
          'description' => 'A better Toolbar.',
          'package' => 'Administration',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'admin_menu',
          ),
          'version' => '7.x-3.0-rc4',
          'project' => 'admin_menu',
          'datestamp' => '1359651687',
          'php' => '5.2.4',
        ),
        'schema_version' => '6300',
        'project' => 'admin_menu',
        'version' => '7.x-3.0-rc4',
      ),
      'admin_menu' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/admin_menu/admin_menu.module',
        'basename' => 'admin_menu.module',
        'name' => 'admin_menu',
        'info' => 
        array (
          'name' => 'Administration menu',
          'description' => 'Provides a dropdown menu to most administrative tasks and other common destinations (to users with the proper permissions).',
          'package' => 'Administration',
          'core' => '7.x',
          'configure' => 'admin/config/administration/admin_menu',
          'dependencies' => 
          array (
            0 => 'system (>7.10)',
          ),
          'files' => 
          array (
            0 => 'tests/admin_menu.test',
          ),
          'version' => '7.x-3.0-rc4',
          'project' => 'admin_menu',
          'datestamp' => '1359651687',
          'php' => '5.2.4',
        ),
        'schema_version' => '7304',
        'project' => 'admin_menu',
        'version' => '7.x-3.0-rc4',
      ),
      'link' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/link/link.module',
        'basename' => 'link.module',
        'name' => 'link',
        'info' => 
        array (
          'name' => 'Link',
          'description' => 'Defines simple link field types.',
          'core' => '7.x',
          'package' => 'Fields',
          'files' => 
          array (
            0 => 'link.module',
            1 => 'link.migrate.inc',
            2 => 'tests/link.test',
            3 => 'tests/link.attribute.test',
            4 => 'tests/link.crud.test',
            5 => 'tests/link.crud_browser.test',
            6 => 'tests/link.token.test',
            7 => 'tests/link.validate.test',
            8 => 'views/link_views_handler_argument_target.inc',
            9 => 'views/link_views_handler_filter_protocol.inc',
          ),
          'version' => '7.x-1.1',
          'project' => 'link',
          'datestamp' => '1360444361',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'link',
        'version' => '7.x-1.1',
      ),
      'views_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/views/tests/views_test.module',
        'basename' => 'views_test.module',
        'name' => 'views_test',
        'info' => 
        array (
          'name' => 'Views Test',
          'description' => 'Test module for Views.',
          'package' => 'Views',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'hidden' => true,
          'version' => '7.x-3.6',
          'project' => 'views',
          'datestamp' => '1363810217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'views',
        'version' => '7.x-3.6',
      ),
      'views_export' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/views/views_export/views_export.module',
        'basename' => 'views_export.module',
        'name' => 'views_export',
        'info' => 
        array (
          'dependencies' => 
          array (
          ),
          'description' => '',
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
      'views_ui' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/views/views_ui.module',
        'basename' => 'views_ui.module',
        'name' => 'views_ui',
        'info' => 
        array (
          'name' => 'Views UI',
          'description' => 'Administrative interface to views. Without this module, you cannot create or edit your views.',
          'package' => 'Views',
          'core' => '7.x',
          'configure' => 'admin/structure/views',
          'dependencies' => 
          array (
            0 => 'views',
          ),
          'files' => 
          array (
            0 => 'views_ui.module',
            1 => 'plugins/views_wizard/views_ui_base_views_wizard.class.php',
          ),
          'version' => '7.x-3.6',
          'project' => 'views',
          'datestamp' => '1363810217',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'views',
        'version' => '7.x-3.6',
      ),
      'views' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/views/views.module',
        'basename' => 'views.module',
        'name' => 'views',
        'info' => 
        array (
          'name' => 'Views',
          'description' => 'Create customized lists and queries from your database.',
          'package' => 'Views',
          'core' => '7.x',
          'php' => '5.2',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/views.css',
            ),
          ),
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'handlers/views_handler_area.inc',
            1 => 'handlers/views_handler_area_result.inc',
            2 => 'handlers/views_handler_area_text.inc',
            3 => 'handlers/views_handler_area_text_custom.inc',
            4 => 'handlers/views_handler_area_view.inc',
            5 => 'handlers/views_handler_argument.inc',
            6 => 'handlers/views_handler_argument_date.inc',
            7 => 'handlers/views_handler_argument_formula.inc',
            8 => 'handlers/views_handler_argument_many_to_one.inc',
            9 => 'handlers/views_handler_argument_null.inc',
            10 => 'handlers/views_handler_argument_numeric.inc',
            11 => 'handlers/views_handler_argument_string.inc',
            12 => 'handlers/views_handler_argument_group_by_numeric.inc',
            13 => 'handlers/views_handler_field.inc',
            14 => 'handlers/views_handler_field_counter.inc',
            15 => 'handlers/views_handler_field_boolean.inc',
            16 => 'handlers/views_handler_field_contextual_links.inc',
            17 => 'handlers/views_handler_field_custom.inc',
            18 => 'handlers/views_handler_field_date.inc',
            19 => 'handlers/views_handler_field_entity.inc',
            20 => 'handlers/views_handler_field_markup.inc',
            21 => 'handlers/views_handler_field_math.inc',
            22 => 'handlers/views_handler_field_numeric.inc',
            23 => 'handlers/views_handler_field_prerender_list.inc',
            24 => 'handlers/views_handler_field_time_interval.inc',
            25 => 'handlers/views_handler_field_serialized.inc',
            26 => 'handlers/views_handler_field_machine_name.inc',
            27 => 'handlers/views_handler_field_url.inc',
            28 => 'handlers/views_handler_filter.inc',
            29 => 'handlers/views_handler_filter_boolean_operator.inc',
            30 => 'handlers/views_handler_filter_boolean_operator_string.inc',
            31 => 'handlers/views_handler_filter_combine.inc',
            32 => 'handlers/views_handler_filter_date.inc',
            33 => 'handlers/views_handler_filter_equality.inc',
            34 => 'handlers/views_handler_filter_entity_bundle.inc',
            35 => 'handlers/views_handler_filter_group_by_numeric.inc',
            36 => 'handlers/views_handler_filter_in_operator.inc',
            37 => 'handlers/views_handler_filter_many_to_one.inc',
            38 => 'handlers/views_handler_filter_numeric.inc',
            39 => 'handlers/views_handler_filter_string.inc',
            40 => 'handlers/views_handler_relationship.inc',
            41 => 'handlers/views_handler_relationship_groupwise_max.inc',
            42 => 'handlers/views_handler_sort.inc',
            43 => 'handlers/views_handler_sort_date.inc',
            44 => 'handlers/views_handler_sort_formula.inc',
            45 => 'handlers/views_handler_sort_group_by_numeric.inc',
            46 => 'handlers/views_handler_sort_menu_hierarchy.inc',
            47 => 'handlers/views_handler_sort_random.inc',
            48 => 'includes/base.inc',
            49 => 'includes/handlers.inc',
            50 => 'includes/plugins.inc',
            51 => 'includes/view.inc',
            52 => 'modules/aggregator/views_handler_argument_aggregator_fid.inc',
            53 => 'modules/aggregator/views_handler_argument_aggregator_iid.inc',
            54 => 'modules/aggregator/views_handler_argument_aggregator_category_cid.inc',
            55 => 'modules/aggregator/views_handler_field_aggregator_title_link.inc',
            56 => 'modules/aggregator/views_handler_field_aggregator_category.inc',
            57 => 'modules/aggregator/views_handler_field_aggregator_item_description.inc',
            58 => 'modules/aggregator/views_handler_field_aggregator_xss.inc',
            59 => 'modules/aggregator/views_handler_filter_aggregator_category_cid.inc',
            60 => 'modules/aggregator/views_plugin_row_aggregator_rss.inc',
            61 => 'modules/book/views_plugin_argument_default_book_root.inc',
            62 => 'modules/comment/views_handler_argument_comment_user_uid.inc',
            63 => 'modules/comment/views_handler_field_comment.inc',
            64 => 'modules/comment/views_handler_field_comment_depth.inc',
            65 => 'modules/comment/views_handler_field_comment_link.inc',
            66 => 'modules/comment/views_handler_field_comment_link_approve.inc',
            67 => 'modules/comment/views_handler_field_comment_link_delete.inc',
            68 => 'modules/comment/views_handler_field_comment_link_edit.inc',
            69 => 'modules/comment/views_handler_field_comment_link_reply.inc',
            70 => 'modules/comment/views_handler_field_comment_node_link.inc',
            71 => 'modules/comment/views_handler_field_comment_username.inc',
            72 => 'modules/comment/views_handler_field_ncs_last_comment_name.inc',
            73 => 'modules/comment/views_handler_field_ncs_last_updated.inc',
            74 => 'modules/comment/views_handler_field_node_comment.inc',
            75 => 'modules/comment/views_handler_field_node_new_comments.inc',
            76 => 'modules/comment/views_handler_field_last_comment_timestamp.inc',
            77 => 'modules/comment/views_handler_filter_comment_user_uid.inc',
            78 => 'modules/comment/views_handler_filter_ncs_last_updated.inc',
            79 => 'modules/comment/views_handler_filter_node_comment.inc',
            80 => 'modules/comment/views_handler_sort_comment_thread.inc',
            81 => 'modules/comment/views_handler_sort_ncs_last_comment_name.inc',
            82 => 'modules/comment/views_handler_sort_ncs_last_updated.inc',
            83 => 'modules/comment/views_plugin_row_comment_rss.inc',
            84 => 'modules/comment/views_plugin_row_comment_view.inc',
            85 => 'modules/contact/views_handler_field_contact_link.inc',
            86 => 'modules/field/views_handler_field_field.inc',
            87 => 'modules/field/views_handler_relationship_entity_reverse.inc',
            88 => 'modules/field/views_handler_argument_field_list.inc',
            89 => 'modules/field/views_handler_argument_field_list_string.inc',
            90 => 'modules/field/views_handler_filter_field_list.inc',
            91 => 'modules/filter/views_handler_field_filter_format_name.inc',
            92 => 'modules/locale/views_handler_field_node_language.inc',
            93 => 'modules/locale/views_handler_filter_node_language.inc',
            94 => 'modules/locale/views_handler_argument_locale_group.inc',
            95 => 'modules/locale/views_handler_argument_locale_language.inc',
            96 => 'modules/locale/views_handler_field_locale_group.inc',
            97 => 'modules/locale/views_handler_field_locale_language.inc',
            98 => 'modules/locale/views_handler_field_locale_link_edit.inc',
            99 => 'modules/locale/views_handler_filter_locale_group.inc',
            100 => 'modules/locale/views_handler_filter_locale_language.inc',
            101 => 'modules/locale/views_handler_filter_locale_version.inc',
            102 => 'modules/node/views_handler_argument_dates_various.inc',
            103 => 'modules/node/views_handler_argument_node_language.inc',
            104 => 'modules/node/views_handler_argument_node_nid.inc',
            105 => 'modules/node/views_handler_argument_node_type.inc',
            106 => 'modules/node/views_handler_argument_node_vid.inc',
            107 => 'modules/node/views_handler_argument_node_uid_revision.inc',
            108 => 'modules/node/views_handler_field_history_user_timestamp.inc',
            109 => 'modules/node/views_handler_field_node.inc',
            110 => 'modules/node/views_handler_field_node_link.inc',
            111 => 'modules/node/views_handler_field_node_link_delete.inc',
            112 => 'modules/node/views_handler_field_node_link_edit.inc',
            113 => 'modules/node/views_handler_field_node_revision.inc',
            114 => 'modules/node/views_handler_field_node_revision_link.inc',
            115 => 'modules/node/views_handler_field_node_revision_link_delete.inc',
            116 => 'modules/node/views_handler_field_node_revision_link_revert.inc',
            117 => 'modules/node/views_handler_field_node_path.inc',
            118 => 'modules/node/views_handler_field_node_type.inc',
            119 => 'modules/node/views_handler_filter_history_user_timestamp.inc',
            120 => 'modules/node/views_handler_filter_node_access.inc',
            121 => 'modules/node/views_handler_filter_node_status.inc',
            122 => 'modules/node/views_handler_filter_node_type.inc',
            123 => 'modules/node/views_handler_filter_node_uid_revision.inc',
            124 => 'modules/node/views_plugin_argument_default_node.inc',
            125 => 'modules/node/views_plugin_argument_validate_node.inc',
            126 => 'modules/node/views_plugin_row_node_rss.inc',
            127 => 'modules/node/views_plugin_row_node_view.inc',
            128 => 'modules/profile/views_handler_field_profile_date.inc',
            129 => 'modules/profile/views_handler_field_profile_list.inc',
            130 => 'modules/profile/views_handler_filter_profile_selection.inc',
            131 => 'modules/search/views_handler_argument_search.inc',
            132 => 'modules/search/views_handler_field_search_score.inc',
            133 => 'modules/search/views_handler_filter_search.inc',
            134 => 'modules/search/views_handler_sort_search_score.inc',
            135 => 'modules/search/views_plugin_row_search_view.inc',
            136 => 'modules/statistics/views_handler_field_accesslog_path.inc',
            137 => 'modules/system/views_handler_argument_file_fid.inc',
            138 => 'modules/system/views_handler_field_file.inc',
            139 => 'modules/system/views_handler_field_file_extension.inc',
            140 => 'modules/system/views_handler_field_file_filemime.inc',
            141 => 'modules/system/views_handler_field_file_uri.inc',
            142 => 'modules/system/views_handler_field_file_status.inc',
            143 => 'modules/system/views_handler_filter_file_status.inc',
            144 => 'modules/taxonomy/views_handler_argument_taxonomy.inc',
            145 => 'modules/taxonomy/views_handler_argument_term_node_tid.inc',
            146 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth.inc',
            147 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth_modifier.inc',
            148 => 'modules/taxonomy/views_handler_argument_vocabulary_vid.inc',
            149 => 'modules/taxonomy/views_handler_argument_vocabulary_machine_name.inc',
            150 => 'modules/taxonomy/views_handler_field_taxonomy.inc',
            151 => 'modules/taxonomy/views_handler_field_term_node_tid.inc',
            152 => 'modules/taxonomy/views_handler_field_term_link_edit.inc',
            153 => 'modules/taxonomy/views_handler_filter_term_node_tid.inc',
            154 => 'modules/taxonomy/views_handler_filter_term_node_tid_depth.inc',
            155 => 'modules/taxonomy/views_handler_filter_vocabulary_vid.inc',
            156 => 'modules/taxonomy/views_handler_filter_vocabulary_machine_name.inc',
            157 => 'modules/taxonomy/views_handler_relationship_node_term_data.inc',
            158 => 'modules/taxonomy/views_plugin_argument_validate_taxonomy_term.inc',
            159 => 'modules/taxonomy/views_plugin_argument_default_taxonomy_tid.inc',
            160 => 'modules/tracker/views_handler_argument_tracker_comment_user_uid.inc',
            161 => 'modules/tracker/views_handler_filter_tracker_comment_user_uid.inc',
            162 => 'modules/tracker/views_handler_filter_tracker_boolean_operator.inc',
            163 => 'modules/system/views_handler_filter_system_type.inc',
            164 => 'modules/translation/views_handler_argument_node_tnid.inc',
            165 => 'modules/translation/views_handler_field_node_link_translate.inc',
            166 => 'modules/translation/views_handler_field_node_translation_link.inc',
            167 => 'modules/translation/views_handler_filter_node_tnid.inc',
            168 => 'modules/translation/views_handler_filter_node_tnid_child.inc',
            169 => 'modules/translation/views_handler_relationship_translation.inc',
            170 => 'modules/user/views_handler_argument_user_uid.inc',
            171 => 'modules/user/views_handler_argument_users_roles_rid.inc',
            172 => 'modules/user/views_handler_field_user.inc',
            173 => 'modules/user/views_handler_field_user_language.inc',
            174 => 'modules/user/views_handler_field_user_link.inc',
            175 => 'modules/user/views_handler_field_user_link_cancel.inc',
            176 => 'modules/user/views_handler_field_user_link_edit.inc',
            177 => 'modules/user/views_handler_field_user_mail.inc',
            178 => 'modules/user/views_handler_field_user_name.inc',
            179 => 'modules/user/views_handler_field_user_permissions.inc',
            180 => 'modules/user/views_handler_field_user_picture.inc',
            181 => 'modules/user/views_handler_field_user_roles.inc',
            182 => 'modules/user/views_handler_filter_user_current.inc',
            183 => 'modules/user/views_handler_filter_user_name.inc',
            184 => 'modules/user/views_handler_filter_user_permissions.inc',
            185 => 'modules/user/views_handler_filter_user_roles.inc',
            186 => 'modules/user/views_plugin_argument_default_current_user.inc',
            187 => 'modules/user/views_plugin_argument_default_user.inc',
            188 => 'modules/user/views_plugin_argument_validate_user.inc',
            189 => 'modules/user/views_plugin_row_user_view.inc',
            190 => 'plugins/views_plugin_access.inc',
            191 => 'plugins/views_plugin_access_none.inc',
            192 => 'plugins/views_plugin_access_perm.inc',
            193 => 'plugins/views_plugin_access_role.inc',
            194 => 'plugins/views_plugin_argument_default.inc',
            195 => 'plugins/views_plugin_argument_default_php.inc',
            196 => 'plugins/views_plugin_argument_default_fixed.inc',
            197 => 'plugins/views_plugin_argument_default_raw.inc',
            198 => 'plugins/views_plugin_argument_validate.inc',
            199 => 'plugins/views_plugin_argument_validate_numeric.inc',
            200 => 'plugins/views_plugin_argument_validate_php.inc',
            201 => 'plugins/views_plugin_cache.inc',
            202 => 'plugins/views_plugin_cache_none.inc',
            203 => 'plugins/views_plugin_cache_time.inc',
            204 => 'plugins/views_plugin_display.inc',
            205 => 'plugins/views_plugin_display_attachment.inc',
            206 => 'plugins/views_plugin_display_block.inc',
            207 => 'plugins/views_plugin_display_default.inc',
            208 => 'plugins/views_plugin_display_embed.inc',
            209 => 'plugins/views_plugin_display_extender.inc',
            210 => 'plugins/views_plugin_display_feed.inc',
            211 => 'plugins/views_plugin_display_page.inc',
            212 => 'plugins/views_plugin_exposed_form_basic.inc',
            213 => 'plugins/views_plugin_exposed_form.inc',
            214 => 'plugins/views_plugin_exposed_form_input_required.inc',
            215 => 'plugins/views_plugin_localization_core.inc',
            216 => 'plugins/views_plugin_localization.inc',
            217 => 'plugins/views_plugin_localization_none.inc',
            218 => 'plugins/views_plugin_pager.inc',
            219 => 'plugins/views_plugin_pager_full.inc',
            220 => 'plugins/views_plugin_pager_mini.inc',
            221 => 'plugins/views_plugin_pager_none.inc',
            222 => 'plugins/views_plugin_pager_some.inc',
            223 => 'plugins/views_plugin_query.inc',
            224 => 'plugins/views_plugin_query_default.inc',
            225 => 'plugins/views_plugin_row.inc',
            226 => 'plugins/views_plugin_row_fields.inc',
            227 => 'plugins/views_plugin_row_rss_fields.inc',
            228 => 'plugins/views_plugin_style.inc',
            229 => 'plugins/views_plugin_style_default.inc',
            230 => 'plugins/views_plugin_style_grid.inc',
            231 => 'plugins/views_plugin_style_list.inc',
            232 => 'plugins/views_plugin_style_jump_menu.inc',
            233 => 'plugins/views_plugin_style_mapping.inc',
            234 => 'plugins/views_plugin_style_rss.inc',
            235 => 'plugins/views_plugin_style_summary.inc',
            236 => 'plugins/views_plugin_style_summary_jump_menu.inc',
            237 => 'plugins/views_plugin_style_summary_unformatted.inc',
            238 => 'plugins/views_plugin_style_table.inc',
            239 => 'tests/handlers/views_handler_area_text.test',
            240 => 'tests/handlers/views_handler_argument_null.test',
            241 => 'tests/handlers/views_handler_argument_string.test',
            242 => 'tests/handlers/views_handler_field.test',
            243 => 'tests/handlers/views_handler_field_boolean.test',
            244 => 'tests/handlers/views_handler_field_custom.test',
            245 => 'tests/handlers/views_handler_field_counter.test',
            246 => 'tests/handlers/views_handler_field_date.test',
            247 => 'tests/handlers/views_handler_field_file_size.test',
            248 => 'tests/handlers/views_handler_field_math.test',
            249 => 'tests/handlers/views_handler_field_url.test',
            250 => 'tests/handlers/views_handler_field_xss.test',
            251 => 'tests/handlers/views_handler_filter_combine.test',
            252 => 'tests/handlers/views_handler_filter_date.test',
            253 => 'tests/handlers/views_handler_filter_equality.test',
            254 => 'tests/handlers/views_handler_filter_in_operator.test',
            255 => 'tests/handlers/views_handler_filter_numeric.test',
            256 => 'tests/handlers/views_handler_filter_string.test',
            257 => 'tests/handlers/views_handler_sort_random.test',
            258 => 'tests/handlers/views_handler_sort_date.test',
            259 => 'tests/handlers/views_handler_sort.test',
            260 => 'tests/test_plugins/views_test_plugin_access_test_dynamic.inc',
            261 => 'tests/test_plugins/views_test_plugin_access_test_static.inc',
            262 => 'tests/test_plugins/views_test_plugin_style_test_mapping.inc',
            263 => 'tests/plugins/views_plugin_display.test',
            264 => 'tests/styles/views_plugin_style_jump_menu.test',
            265 => 'tests/styles/views_plugin_style.test',
            266 => 'tests/styles/views_plugin_style_base.test',
            267 => 'tests/styles/views_plugin_style_mapping.test',
            268 => 'tests/styles/views_plugin_style_unformatted.test',
            269 => 'tests/views_access.test',
            270 => 'tests/views_analyze.test',
            271 => 'tests/views_basic.test',
            272 => 'tests/views_argument_default.test',
            273 => 'tests/views_argument_validator.test',
            274 => 'tests/views_exposed_form.test',
            275 => 'tests/field/views_fieldapi.test',
            276 => 'tests/views_glossary.test',
            277 => 'tests/views_groupby.test',
            278 => 'tests/views_handlers.test',
            279 => 'tests/views_module.test',
            280 => 'tests/views_pager.test',
            281 => 'tests/views_plugin_localization_test.inc',
            282 => 'tests/views_translatable.test',
            283 => 'tests/views_query.test',
            284 => 'tests/views_upgrade.test',
            285 => 'tests/views_test.views_default.inc',
            286 => 'tests/comment/views_handler_argument_comment_user_uid.test',
            287 => 'tests/comment/views_handler_filter_comment_user_uid.test',
            288 => 'tests/node/views_node_revision_relations.test',
            289 => 'tests/taxonomy/views_handler_relationship_node_term_data.test',
            290 => 'tests/user/views_handler_field_user_name.test',
            291 => 'tests/user/views_user_argument_default.test',
            292 => 'tests/user/views_user_argument_validate.test',
            293 => 'tests/user/views_user.test',
            294 => 'tests/views_cache.test',
            295 => 'tests/views_view.test',
            296 => 'tests/views_ui.test',
          ),
          'version' => '7.x-3.6',
          'project' => 'views',
          'datestamp' => '1363810217',
        ),
        'schema_version' => '7301',
        'project' => 'views',
        'version' => '7.x-3.6',
      ),
      'features_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/features/tests/features_test.module',
        'basename' => 'features_test.module',
        'name' => 'features_test',
        'info' => 
        array (
          'name' => 'Features Tests',
          'description' => 'Test module for Features testing.',
          'core' => '7.x',
          'package' => 'Testing',
          'php' => '5.2.0',
          'dependencies' => 
          array (
            0 => 'features',
            1 => 'image',
            2 => 'strongarm',
            3 => 'taxonomy',
            4 => 'views',
          ),
          'features' => 
          array (
            'ctools' => 
            array (
              0 => 'strongarm:strongarm:1',
              1 => 'views:views_default:3.0',
            ),
            'features_api' => 
            array (
              0 => 'api:1',
            ),
            'field' => 
            array (
              0 => 'node-features_test-field_features_test',
            ),
            'filter' => 
            array (
              0 => 'features_test',
            ),
            'image' => 
            array (
              0 => 'features_test',
            ),
            'node' => 
            array (
              0 => 'features_test',
            ),
            'taxonomy' => 
            array (
              0 => 'taxonomy_features_test',
            ),
            'user_permission' => 
            array (
              0 => 'create features_test content',
            ),
            'views_view' => 
            array (
              0 => 'features_test',
            ),
          ),
          'hidden' => true,
          'version' => '7.x-2.0-beta2',
          'project' => 'features',
          'datestamp' => '1364589018',
        ),
        'schema_version' => 0,
        'project' => 'features',
        'version' => '7.x-2.0-beta2',
      ),
      'features' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/features/features.module',
        'basename' => 'features.module',
        'name' => 'features',
        'info' => 
        array (
          'name' => 'Features',
          'description' => 'Provides feature management for Drupal.',
          'core' => '7.x',
          'package' => 'Features',
          'files' => 
          array (
            0 => 'tests/features.test',
          ),
          'version' => '7.x-2.0-beta2',
          'project' => 'features',
          'datestamp' => '1364589018',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '6101',
        'project' => 'features',
        'version' => '7.x-2.0-beta2',
      ),
      'date_popup' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_popup/date_popup.module',
        'basename' => 'date_popup.module',
        'name' => 'date_popup',
        'info' => 
        array (
          'name' => 'Date Popup',
          'description' => 'Enables jquery popup calendars and time entry widgets for selecting dates and times.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'configure' => 'admin/config/date/date_popup',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'themes/datepicker.1.7.css',
            ),
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_tools' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_tools/date_tools.module',
        'basename' => 'date_tools.module',
        'name' => 'date_tools',
        'info' => 
        array (
          'name' => 'Date Tools',
          'description' => 'Tools to import and auto-create dates and calendars.',
          'dependencies' => 
          array (
            0 => 'date',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'configure' => 'admin/config/date/tools',
          'files' => 
          array (
            0 => 'tests/date_tools.test',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_views' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_views/date_views.module',
        'basename' => 'date_views.module',
        'name' => 'date_views',
        'info' => 
        array (
          'name' => 'Date Views',
          'description' => 'Views integration for date fields and date functionality.',
          'package' => 'Date/Time',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'views',
          ),
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'includes/date_views_argument_handler.inc',
            1 => 'includes/date_views_argument_handler_simple.inc',
            2 => 'includes/date_views_filter_handler.inc',
            3 => 'includes/date_views_filter_handler_simple.inc',
            4 => 'includes/date_views.views_default.inc',
            5 => 'includes/date_views.views.inc',
            6 => 'includes/date_views_plugin_pager.inc',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_context' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_context/date_context.module',
        'basename' => 'date_context.module',
        'name' => 'date_context',
        'info' => 
        array (
          'name' => 'Date Context',
          'description' => 'Adds an option to the Context module to set a context condition based on the value of a date field.',
          'package' => 'Date/Time',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'date',
            1 => 'context',
          ),
          'files' => 
          array (
            0 => 'date_context.module',
            1 => 'plugins/date_context_date_condition.inc',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_migrate_example' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_migrate/date_migrate_example/date_migrate_example.module',
        'basename' => 'date_migrate_example.module',
        'name' => 'date_migrate_example',
        'info' => 
        array (
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'date',
            1 => 'date_repeat',
            2 => 'date_repeat_field',
            3 => 'date_migrate',
            4 => 'features',
            5 => 'migrate',
          ),
          'description' => 'Examples of migrating with the Date module',
          'features' => 
          array (
            'field' => 
            array (
              0 => 'node-date_migrate_example-body',
              1 => 'node-date_migrate_example-field_date',
              2 => 'node-date_migrate_example-field_date_range',
              3 => 'node-date_migrate_example-field_date_repeat',
              4 => 'node-date_migrate_example-field_datestamp',
              5 => 'node-date_migrate_example-field_datestamp_range',
              6 => 'node-date_migrate_example-field_datetime',
              7 => 'node-date_migrate_example-field_datetime_range',
            ),
            'node' => 
            array (
              0 => 'date_migrate_example',
            ),
          ),
          'files' => 
          array (
            0 => 'date_migrate_example.migrate.inc',
          ),
          'name' => 'Date Migration Example',
          'package' => 'Features',
          'project' => 'date',
          'version' => '7.x-2.6',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_migrate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_migrate/date_migrate.module',
        'basename' => 'date_migrate.module',
        'name' => 'date_migrate',
        'info' => 
        array (
          'name' => 'Date Migration',
          'description' => 'Provides support for importing into date fields with the Migrate module.',
          'core' => '7.x',
          'package' => 'Date/Time',
          'dependencies' => 
          array (
            0 => 'migrate',
            1 => 'date',
          ),
          'files' => 
          array (
            0 => 'date.migrate.inc',
            1 => 'date_migrate.test',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_repeat_field' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_repeat_field/date_repeat_field.module',
        'basename' => 'date_repeat_field.module',
        'name' => 'date_repeat_field',
        'info' => 
        array (
          'name' => 'Date Repeat Field',
          'description' => 'Creates the option of Repeating date fields and manages Date fields that use the Date Repeat API.',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'date',
            2 => 'date_repeat',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'date_repeat_field.css',
            ),
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_repeat' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_repeat/date_repeat.module',
        'basename' => 'date_repeat.module',
        'name' => 'date_repeat',
        'info' => 
        array (
          'name' => 'Date Repeat API',
          'description' => 'A Date Repeat API to calculate repeating dates and times from iCal rules.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'tests/date_repeat.test',
            1 => 'tests/date_repeat_form.test',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_all_day' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_all_day/date_all_day.module',
        'basename' => 'date_all_day.module',
        'name' => 'date_all_day',
        'info' => 
        array (
          'name' => 'Date All Day',
          'description' => 'Adds \'All Day\' functionality to date fields, including an \'All Day\' theme and \'All Day\' checkboxes for the Date select and Date popup widgets.',
          'dependencies' => 
          array (
            0 => 'date_api',
            1 => 'date',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date_api' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date_api/date_api.module',
        'basename' => 'date_api.module',
        'name' => 'date_api',
        'info' => 
        array (
          'name' => 'Date API',
          'description' => 'A Date API that can be used by other modules.',
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'date.css',
            ),
          ),
          'files' => 
          array (
            0 => 'date_api.module',
            1 => 'date_api_sql.inc',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
          'dependencies' => 
          array (
          ),
        ),
        'schema_version' => '7001',
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'date' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/date/date.module',
        'basename' => 'date.module',
        'name' => 'date',
        'info' => 
        array (
          'name' => 'Date',
          'description' => 'Makes date/time fields available.',
          'dependencies' => 
          array (
            0 => 'date_api',
          ),
          'package' => 'Date/Time',
          'core' => '7.x',
          'php' => '5.2',
          'files' => 
          array (
            0 => 'tests/date_api.test',
            1 => 'tests/date.test',
            2 => 'tests/date_field.test',
            3 => 'tests/date_validation.test',
            4 => 'tests/date_timezone.test',
          ),
          'version' => '7.x-2.6',
          'project' => 'date',
          'datestamp' => '1344850024',
        ),
        'schema_version' => '7004',
        'project' => 'date',
        'version' => '7.x-2.6',
      ),
      'devel_generate' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/devel/devel_generate/devel_generate.module',
        'basename' => 'devel_generate.module',
        'name' => 'devel_generate',
        'info' => 
        array (
          'name' => 'Devel generate',
          'description' => 'Generate dummy users, nodes, and taxonomy terms.',
          'package' => 'Development',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'devel',
          ),
          'tags' => 
          array (
            0 => 'developer',
          ),
          'configure' => 'admin/config/development/generate',
          'version' => '7.x-1.3',
          'project' => 'devel',
          'datestamp' => '1338940281',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'devel',
        'version' => '7.x-1.3',
      ),
      'devel' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/devel/devel.module',
        'basename' => 'devel.module',
        'name' => 'devel',
        'info' => 
        array (
          'name' => 'Devel',
          'description' => 'Various blocks, pages, and functions for developers.',
          'package' => 'Development',
          'core' => '7.x',
          'configure' => 'admin/config/development/devel',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'files' => 
          array (
            0 => 'devel.test',
            1 => 'devel.mail.inc',
          ),
          'version' => '7.x-1.3',
          'project' => 'devel',
          'datestamp' => '1338940281',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'project' => 'devel',
        'version' => '7.x-1.3',
      ),
      'devel_node_access' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/devel/devel_node_access.module',
        'basename' => 'devel_node_access.module',
        'name' => 'devel_node_access',
        'info' => 
        array (
          'name' => 'Devel node access',
          'description' => 'Developer blocks and page illustrating relevant node_access records.',
          'package' => 'Development',
          'dependencies' => 
          array (
            0 => 'menu',
          ),
          'core' => '7.x',
          'configure' => 'admin/config/development/devel',
          'tags' => 
          array (
            0 => 'developer',
          ),
          'version' => '7.x-1.3',
          'project' => 'devel',
          'datestamp' => '1338940281',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'devel',
        'version' => '7.x-1.3',
      ),
      'token_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/token/tests/token_test.module',
        'basename' => 'token_test.module',
        'name' => 'token_test',
        'info' => 
        array (
          'name' => 'Token Test',
          'description' => 'Testing module for token functionality.',
          'package' => 'Testing',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'token_test.module',
          ),
          'hidden' => true,
          'version' => '7.x-1.5',
          'project' => 'token',
          'datestamp' => '1361665026',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'token',
        'version' => '7.x-1.5',
      ),
      'token' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/token/token.module',
        'basename' => 'token.module',
        'name' => 'token',
        'info' => 
        array (
          'name' => 'Token',
          'description' => 'Provides a user interface for the Token API and some missing core tokens.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'token.test',
          ),
          'version' => '7.x-1.5',
          'project' => 'token',
          'datestamp' => '1361665026',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'project' => 'token',
        'version' => '7.x-1.5',
      ),
      'better_formats' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/better_formats/better_formats.module',
        'basename' => 'better_formats.module',
        'name' => 'better_formats',
        'info' => 
        array (
          'name' => 'Better Formats',
          'description' => 'Enhances the core input format system by managing input format defaults and settings.',
          'core' => '7.x',
          'configure' => 'admin/config/content/formats',
          'version' => '7.x-1.0-beta1',
          'project' => 'better_formats',
          'datestamp' => '1343262404',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'project' => 'better_formats',
        'version' => '7.x-1.0-beta1',
      ),
      'libraries_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/libraries/tests/libraries_test.module',
        'basename' => 'libraries_test.module',
        'name' => 'libraries_test',
        'info' => 
        array (
          'name' => 'Libraries test module',
          'description' => 'Tests library detection and loading.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'libraries',
          ),
          'hidden' => true,
          'version' => '7.x-2.1',
          'project' => 'libraries',
          'datestamp' => '1362848412',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'libraries',
        'version' => '7.x-2.1',
      ),
      'libraries' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/libraries/libraries.module',
        'basename' => 'libraries.module',
        'name' => 'libraries',
        'info' => 
        array (
          'name' => 'Libraries',
          'description' => 'Allows version-dependent and shared usage of external libraries.',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tests/libraries.test',
          ),
          'version' => '7.x-2.1',
          'project' => 'libraries',
          'datestamp' => '1362848412',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7200',
        'project' => 'libraries',
        'version' => '7.x-2.1',
      ),
      'select_or_other' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/select_or_other/select_or_other.module',
        'basename' => 'select_or_other.module',
        'name' => 'select_or_other',
        'info' => 
        array (
          'name' => 'Select (or other)',
          'description' => 'Provides a select box form element with additional option \'Other\' to give a textfield.',
          'core' => '7.x',
          'version' => '7.x-2.15',
          'project' => 'select_or_other',
          'datestamp' => '1345436315',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'select_or_other',
        'version' => '7.x-2.15',
      ),
      'admin' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/admin/admin.module',
        'basename' => 'admin.module',
        'name' => 'admin',
        'info' => 
        array (
          'name' => 'Admin',
          'description' => 'UI helpers for Drupal admins and managers.',
          'package' => 'Administration',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'admin.admin.inc',
            1 => 'admin.install',
            2 => 'admin.module',
            3 => 'includes/admin.devel.inc',
            4 => 'includes/admin.theme.inc',
            5 => 'theme/admin-panes.tpl.php',
            6 => 'theme/admin-toolbar.tpl.php',
            7 => 'theme/theme.inc',
          ),
          'version' => '7.x-2.0-beta3',
          'project' => 'admin',
          'datestamp' => '1292541646',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'admin',
        'version' => '7.x-2.0-beta3',
      ),
      'rel_test' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/rel/tests/rel_test.module',
        'basename' => 'rel_test.module',
        'name' => 'rel_test',
        'info' => 
        array (
          'name' => 'Rendereable elements Test',
          'description' => 'Test module for renderable elements',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'rel',
          ),
          'hidden' => true,
          'version' => '7.x-1.0-alpha2',
          'project' => 'rel',
          'datestamp' => '1358839857',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => 'rel',
        'version' => '7.x-1.0-alpha2',
      ),
      'rel' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/contrib/rel/rel.module',
        'basename' => 'rel.module',
        'name' => 'rel',
        'info' => 
        array (
          'name' => 'Renderable elements',
          'description' => 'Register any forms to enable you to manage the display through an UI',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'ctools',
          ),
          'files' => 
          array (
            0 => 'rel.module',
            1 => 'rel.test',
            2 => 'rel.admin.inc',
          ),
          'version' => '7.x-1.0-alpha2',
          'project' => 'rel',
          'datestamp' => '1358839857',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'project' => 'rel',
        'version' => '7.x-1.0-alpha2',
      ),
      'gdp_fields' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/modules/custom/gdp_fields/gdp_fields.module',
        'basename' => 'gdp_fields.module',
        'name' => 'gdp_fields',
        'info' => 
        array (
          'name' => 'GDP Fields',
          'description' => 'TODO: Description of module',
          'package' => 'gdp',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'gdp_fields.module',
            1 => 'gdp_fields.fields.inc',
            2 => 'gdp_fields.install',
            3 => 'gdp_fields/views/gdp_fields.views.inc',
          ),
          'dependencies' => 
          array (
            0 => 'list',
            1 => 'select_or_other',
            2 => 'views',
            3 => 'views_arguments_in_filters',
            4 => 'date',
          ),
          'version' => NULL,
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'project' => '',
        'version' => NULL,
      ),
    ),
    'themes' => 
    array (
      'rubik' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/themes/rubik/rubik.info',
        'basename' => 'rubik.info',
        'name' => 'Rubik',
        'info' => 
        array (
          'name' => 'Rubik',
          'description' => 'Clean admin theme.',
          'base theme' => 'tao',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'regions' => 
          array (
            'content' => 'Content',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
          ),
          'scripts' => 
          array (
            0 => 'js/rubik.js',
          ),
          'stylesheets' => 
          array (
            'screen' => 
            array (
              0 => 'core.css',
              1 => 'icons.css',
              2 => 'style.css',
            ),
            'print' => 
            array (
              0 => 'print.css',
            ),
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '1',
          ),
          'version' => '7.x-4.0-beta8',
          'project' => 'rubik',
          'datestamp' => '1329953445',
        ),
        'project' => 'rubik',
        'version' => '7.x-4.0-beta8',
      ),
      'alpha' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/themes/omega/alpha/alpha.info',
        'basename' => 'alpha.info',
        'name' => 'Alpha',
        'info' => 
        array (
          'name' => 'Alpha',
          'description' => 'Alpha is the core basetheme for <a href="http://drupal.org/project/omega">Omega</a> and all its subthemes. It includes the most basic features of the Omega theme framework. This theme should not be used directly, instead you should create a subtheme based on one of the Omega or Alpha starterkits. Learn more about <a href="http://drupal.org/node/819170">Creating an Omega Subtheme</a> in the <a href="http://drupal.org/node/819164">Omega Handbook</a>.',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'screenshot' => 'screenshot.png',
          'version' => '7.x-3.1',
          'regions' => 
          array (
            'page_top' => 'Page Top',
            'page_bottom' => 'Page Bottom',
            'content' => 'Content',
            'header' => 'Header',
            'footer' => 'Footer',
            'sidebar_first' => 'Sidebar First',
            'sidebar_second' => 'Sidebar Second',
          ),
          'zones' => 
          array (
            'content' => 'Content',
            'header' => 'Header',
            'footer' => 'Footer',
          ),
          'css' => 
          array (
            'alpha-reset.css' => 
            array (
              'name' => 'Reset',
              'description' => 'Created by <a href="http://meyerweb.com/eric/tools/css/reset/">Eric Meyer</a>.',
              'options' => 
              array (
                'weight' => '-20',
              ),
            ),
            'alpha-mobile.css' => 
            array (
              'name' => 'Mobile',
              'description' => 'Default stylesheet for mobile styles.',
              'options' => 
              array (
                'weight' => '-20',
              ),
            ),
            'alpha-alpha.css' => 
            array (
              'name' => 'Alpha',
              'description' => 'Default styles & resets for Alpha/Omega base theme.',
              'options' => 
              array (
                'weight' => '-20',
              ),
            ),
          ),
          'exclude' => 
          array (
            'misc/vertical-tabs.css' => 'This requires a description.',
            'modules/aggregator/aggregator.css' => 'This requires a description.',
            'modules/block/block.css' => 'This requires a description.',
            'modules/dblog/dblog.css' => 'This requires a description.',
            'modules/file/file.css' => 'This requires a description.',
            'modules/filter/filter.css' => 'This requires a description.',
            'modules/help/help.css' => 'This requires a description.',
            'modules/menu/menu.css' => 'This requires a description.',
            'modules/openid/openid.css' => 'This requires a description.',
            'modules/profile/profile.css' => 'This requires a description.',
            'modules/statistics/statistics.css' => 'This requires a description.',
            'modules/syslog/syslog.css' => 'This requires a description.',
            'modules/system/admin.css' => 'This requires a description.',
            'modules/system/maintenance.css' => 'This requires a description.',
            'modules/system/system.css' => 'This requires a description.',
            'modules/system/system.admin.css' => 'This requires a description.',
            'modules/system/system.base.css' => 'This requires a description.',
            'modules/system/system.maintenance.css' => 'This requires a description.',
            'modules/system/system.menus.css' => 'This requires a description.',
            'modules/system/system.messages.css' => 'This requires a description.',
            'modules/system/system.theme.css' => 'This requires a description.',
            'modules/taxonomy/taxonomy.css' => 'This requires a description.',
            'modules/tracker/tracker.css' => 'This requires a description.',
            'modules/update/update.css' => 'This requires a description.',
          ),
          'grids' => 
          array (
            'alpha_default' => 
            array (
              'name' => 'Default (960px)',
              'layouts' => 
              array (
                'fluid' => 'Fluid',
                'narrow' => 'Narrow',
                'normal' => 'Normal',
                'wide' => 'Wide',
              ),
              'columns' => 
              array (
                12 => '12 Columns',
                16 => '16 Columns',
                24 => '24 Columns',
              ),
            ),
            'alpha_fluid' => 
            array (
              'name' => 'Fluid',
              'layouts' => 
              array (
                'normal' => 'Normal',
              ),
              'columns' => 
              array (
                12 => '12 Columns',
                16 => '16 Columns',
                24 => '24 Columns',
              ),
            ),
          ),
          'settings' => 
          array (
            'alpha_grid' => 'alpha_default',
            'alpha_primary_alpha_default' => 'normal',
            'alpha_responsive' => '1',
            'alpha_layouts_alpha_fluid_primary' => 'normal',
            'alpha_layouts_alpha_fluid_normal_responsive' => '0',
            'alpha_layouts_alpha_fluid_normal_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_primary' => 'normal',
            'alpha_layouts_alpha_default_fluid_responsive' => '0',
            'alpha_layouts_alpha_default_fluid_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_fluid_weight' => '0',
            'alpha_layouts_alpha_default_narrow_responsive' => '1',
            'alpha_layouts_alpha_default_narrow_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_narrow_weight' => '1',
            'alpha_layouts_alpha_default_normal_responsive' => '1',
            'alpha_layouts_alpha_default_normal_media' => 'all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_normal_weight' => '2',
            'alpha_layouts_alpha_default_wide_responsive' => '1',
            'alpha_layouts_alpha_default_wide_media' => 'all and (min-width: 1220px)',
            'alpha_layouts_alpha_default_wide_weight' => '3',
            'alpha_viewport' => '1',
            'alpha_viewport_initial_scale' => '1',
            'alpha_viewport_min_scale' => '1',
            'alpha_viewport_max_scale' => '1',
            'alpha_viewport_user_scaleable' => '',
            'alpha_css' => 
            array (
              'alpha-reset.css' => 'alpha-reset.css',
              'alpha-alpha.css' => 'alpha-alpha.css',
              'alpha-mobile.css' => 'alpha-mobile.css',
            ),
            'alpha_debug_block_toggle' => '1',
            'alpha_debug_block_active' => '1',
            'alpha_debug_grid_toggle' => '1',
            'alpha_debug_grid_active' => '1',
            'alpha_debug_grid_roles' => 
            array (
              1 => '1',
              2 => '2',
              3 => '3',
            ),
            'alpha_toggle_messages' => '1',
            'alpha_toggle_action_links' => '1',
            'alpha_toggle_tabs' => '1',
            'alpha_toggle_breadcrumb' => '1',
            'alpha_toggle_page_title' => '1',
            'alpha_toggle_feed_icons' => '1',
            'alpha_hidden_title' => '',
            'alpha_hidden_site_name' => '',
            'alpha_hidden_site_slogan' => '',
            'alpha_zone_header_wrapper' => '',
            'alpha_zone_header_force' => '',
            'alpha_zone_header_section' => 'header',
            'alpha_zone_header_weight' => '',
            'alpha_zone_header_columns' => '12',
            'alpha_zone_header_primary' => '',
            'alpha_zone_header_css' => '',
            'alpha_zone_header_wrapper_css' => '',
            'alpha_zone_content_wrapper' => '',
            'alpha_zone_content_force' => '',
            'alpha_zone_content_section' => 'content',
            'alpha_zone_content_weight' => '',
            'alpha_zone_content_columns' => '12',
            'alpha_zone_content_primary' => '',
            'alpha_zone_content_css' => '',
            'alpha_zone_content_wrapper_css' => '',
            'alpha_zone_footer_wrapper' => '',
            'alpha_zone_footer_force' => '',
            'alpha_zone_footer_section' => 'footer',
            'alpha_zone_footer_weight' => '',
            'alpha_zone_footer_columns' => '12',
            'alpha_zone_footer_primary' => '',
            'alpha_zone_footer_css' => '',
            'alpha_zone_footer_wrapper_css' => '',
            'alpha_region_dashboard_inactive_force' => '',
            'alpha_region_dashboard_inactive_zone' => '',
            'alpha_region_dashboard_inactive_prefix' => '',
            'alpha_region_dashboard_inactive_columns' => '1',
            'alpha_region_dashboard_inactive_suffix' => '',
            'alpha_region_dashboard_inactive_weight' => '',
            'alpha_region_dashboard_inactive_css' => '',
            'alpha_region_dashboard_sidebar_force' => '',
            'alpha_region_dashboard_sidebar_zone' => '',
            'alpha_region_dashboard_sidebar_prefix' => '',
            'alpha_region_dashboard_sidebar_columns' => '1',
            'alpha_region_dashboard_sidebar_suffix' => '',
            'alpha_region_dashboard_sidebar_weight' => '',
            'alpha_region_dashboard_sidebar_css' => '',
            'alpha_region_dashboard_main_force' => '',
            'alpha_region_dashboard_main_zone' => '',
            'alpha_region_dashboard_main_prefix' => '',
            'alpha_region_dashboard_main_columns' => '1',
            'alpha_region_dashboard_main_suffix' => '',
            'alpha_region_dashboard_main_weight' => '',
            'alpha_region_dashboard_main_css' => '',
            'alpha_region_header_force' => '',
            'alpha_region_header_zone' => 'header',
            'alpha_region_header_prefix' => '',
            'alpha_region_header_columns' => '12',
            'alpha_region_header_suffix' => '',
            'alpha_region_header_weight' => '',
            'alpha_region_header_css' => '',
            'alpha_region_content_force' => '',
            'alpha_region_content_zone' => 'content',
            'alpha_region_content_prefix' => '',
            'alpha_region_content_columns' => '6',
            'alpha_region_content_suffix' => '',
            'alpha_region_content_weight' => '1',
            'alpha_region_content_css' => '',
            'alpha_region_sidebar_first_force' => '',
            'alpha_region_sidebar_first_zone' => 'content',
            'alpha_region_sidebar_first_prefix' => '',
            'alpha_region_sidebar_first_columns' => '3',
            'alpha_region_sidebar_first_suffix' => '',
            'alpha_region_sidebar_first_weight' => '2',
            'alpha_region_sidebar_first_css' => '',
            'alpha_region_sidebar_second_force' => '',
            'alpha_region_sidebar_second_zone' => 'content',
            'alpha_region_sidebar_second_prefix' => '',
            'alpha_region_sidebar_second_columns' => '3',
            'alpha_region_sidebar_second_suffix' => '',
            'alpha_region_sidebar_second_weight' => '3',
            'alpha_region_sidebar_second_css' => '',
            'alpha_region_footer_force' => '',
            'alpha_region_footer_zone' => 'footer',
            'alpha_region_footer_prefix' => '',
            'alpha_region_footer_columns' => '12',
            'alpha_region_footer_suffix' => '',
            'alpha_region_footer_weight' => '',
            'alpha_region_footer_css' => '',
          ),
          'project' => 'omega',
          'datestamp' => '1329681647',
        ),
        'project' => 'omega',
        'version' => '7.x-3.1',
      ),
      'omega' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/themes/omega/omega/omega.info',
        'basename' => 'omega.info',
        'name' => 'Omega',
        'info' => 
        array (
          'name' => 'Omega',
          'description' => '<a href="http://drupal.org/project/omega">Omega</a> extends the Omega theme framework with some additional features and makes them availabe to its subthemes. This theme should not be used directly, instead you should create a subtheme based on one of the Omega or Alpha starterkits. Learn more about <a href="http://drupal.org/node/819170">Creating an Omega Subtheme</a> in the <a href="http://drupal.org/node/819164">Omega Handbook</a>.',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'screenshot' => 'screenshot.png',
          'version' => '7.x-3.1',
          'base theme' => 'alpha',
          'regions' => 
          array (
            'page_top' => 'Page Top',
            'page_bottom' => 'Page Bottom',
            'content' => 'Content',
            'user_first' => 'User Bar First',
            'user_second' => 'User Bar Second',
            'branding' => 'Branding',
            'menu' => 'Menu',
            'sidebar_first' => 'Sidebar First',
            'sidebar_second' => 'Sidebar Second',
            'header_first' => 'Header First',
            'header_second' => 'Header Second',
            'preface_first' => 'Preface First',
            'preface_second' => 'Preface Second',
            'preface_third' => 'Preface Third',
            'postscript_first' => 'Postscript First',
            'postscript_second' => 'Postscript Second',
            'postscript_third' => 'Postscript Third',
            'postscript_fourth' => 'Postscript Fourth',
            'footer_first' => 'Footer First',
            'footer_second' => 'Footer Second',
          ),
          'zones' => 
          array (
            'user' => 'User',
            'branding' => 'Branding',
            'menu' => 'Menu',
            'header' => 'Header',
            'preface' => 'Preface',
            'content' => 'Content',
            'postscript' => 'Postscript',
            'footer' => 'Footer',
          ),
          'css' => 
          array (
            'omega-text.css' => 
            array (
              'name' => 'Text Styles',
              'description' => 'Default text styles for Omega.',
              'options' => 
              array (
                'weight' => '-10',
              ),
            ),
            'omega-branding.css' => 
            array (
              'name' => 'Branding Styles',
              'description' => 'Provides positioning for the logo, title and slogan.',
              'options' => 
              array (
                'weight' => '-10',
              ),
            ),
            'omega-menu.css' => 
            array (
              'name' => 'Menu Styles',
              'description' => 'Provides positoning and basic CSS for primary and secondary menus.',
              'options' => 
              array (
                'weight' => '-10',
              ),
            ),
            'omega-forms.css' => 
            array (
              'name' => 'Form Styles',
              'description' => 'Provides basic form styles.',
              'options' => 
              array (
                'weight' => '-10',
              ),
            ),
            'omega-visuals.css' => 
            array (
              'name' => 'Omega Styles',
              'description' => 'Custom visual styles for Omega. (pagers, menus, etc.)',
              'options' => 
              array (
                'weight' => '-10',
              ),
            ),
          ),
          'libraries' => 
          array (
            'omega_formalize' => 
            array (
              'name' => 'Formalize',
              'description' => 'Formalize is a framework by <a href="http://formalize.me/" title="Formalize">Nathan Smith</a> for neat looking cross-browser forms with extended functionality.',
              'js' => 
              array (
                0 => 
                array (
                  'file' => 'jquery.formalize.js',
                  'options' => 
                  array (
                    'weight' => '-20',
                  ),
                ),
              ),
              'css' => 
              array (
                0 => 
                array (
                  'file' => 'formalize.css',
                  'options' => 
                  array (
                    'weight' => '-20',
                  ),
                ),
              ),
            ),
            'omega_mediaqueries' => 
            array (
              'name' => 'Media queries',
              'description' => 'Provides a tiny JavaScript library that can be used in your custom JavaScript.',
              'js' => 
              array (
                0 => 
                array (
                  'file' => 'omega-mediaqueries.js',
                  'options' => 
                  array (
                    'weight' => '-19',
                  ),
                ),
              ),
            ),
            'omega_equalheights' => 
            array (
              'name' => 'Equal heights',
              'description' => 'Allows you to force all regions of a zone or all blocks of a region to be of equal size. <span class="marker">This library reveals a corresponding checkbox on every region and zone configuration panel if activated.</span>',
              'js' => 
              array (
                0 => 
                array (
                  'file' => 'omega-equalheights.js',
                  'options' => 
                  array (
                    'weight' => '-18',
                  ),
                ),
              ),
            ),
          ),
          'plugins' => 
          array (
            'panels' => 
            array (
              'layouts' => 'panels/layouts',
            ),
          ),
          'settings' => 
          array (
            'alpha_grid' => 'alpha_default',
            'alpha_primary_alpha_default' => 'normal',
            'alpha_responsive' => '1',
            'alpha_layouts_alpha_fluid_primary' => 'normal',
            'alpha_layouts_alpha_fluid_normal_responsive' => '0',
            'alpha_layouts_alpha_fluid_normal_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_primary' => 'normal',
            'alpha_layouts_alpha_default_fluid_responsive' => '0',
            'alpha_layouts_alpha_default_fluid_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_fluid_weight' => '0',
            'alpha_layouts_alpha_default_narrow_responsive' => '1',
            'alpha_layouts_alpha_default_narrow_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_narrow_weight' => '1',
            'alpha_layouts_alpha_default_normal_responsive' => '1',
            'alpha_layouts_alpha_default_normal_media' => 'all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_normal_weight' => '2',
            'alpha_layouts_alpha_default_wide_responsive' => '1',
            'alpha_layouts_alpha_default_wide_media' => 'all and (min-width: 1220px)',
            'alpha_layouts_alpha_default_wide_weight' => '3',
            'alpha_viewport' => '1',
            'alpha_viewport_initial_scale' => '1',
            'alpha_viewport_min_scale' => '1',
            'alpha_viewport_max_scale' => '1',
            'alpha_viewport_user_scaleable' => '',
            'alpha_libraries' => 
            array (
              'omega_formalize' => 'omega_formalize',
              'omega_equalheights' => '',
              'omega_mediaqueries' => 'omega_mediaqueries',
            ),
            'alpha_css' => 
            array (
              'alpha-reset.css' => 'alpha-reset.css',
              'alpha-mobile.css' => 'alpha-mobile.css',
              'alpha-alpha.css' => 'alpha-alpha.css',
              'omega-text.css' => 'omega-text.css',
              'omega-branding.css' => 'omega-branding.css',
              'omega-menu.css' => 'omega-menu.css',
              'omega-forms.css' => 'omega-forms.css',
              'omega-visuals.css' => 'omega-visuals.css',
            ),
            'alpha_debug_block_toggle' => '1',
            'alpha_debug_block_active' => '1',
            'alpha_debug_grid_toggle' => '1',
            'alpha_debug_grid_active' => '1',
            'alpha_debug_grid_roles' => 
            array (
              1 => '1',
              2 => '2',
              3 => '3',
            ),
            'alpha_toggle_messages' => '1',
            'alpha_toggle_action_links' => '1',
            'alpha_toggle_tabs' => '1',
            'alpha_toggle_breadcrumb' => '1',
            'alpha_toggle_page_title' => '1',
            'alpha_toggle_feed_icons' => '1',
            'alpha_hidden_title' => '',
            'alpha_hidden_site_name' => '',
            'alpha_hidden_site_slogan' => '',
            'alpha_zone_user_equal_height_container' => '',
            'alpha_zone_user_wrapper' => '1',
            'alpha_zone_user_force' => '',
            'alpha_zone_user_section' => 'header',
            'alpha_zone_user_weight' => '1',
            'alpha_zone_user_columns' => '12',
            'alpha_zone_user_primary' => '',
            'alpha_zone_user_css' => '',
            'alpha_zone_user_wrapper_css' => '',
            'alpha_zone_branding_equal_height_container' => '',
            'alpha_zone_branding_wrapper' => '1',
            'alpha_zone_branding_force' => '',
            'alpha_zone_branding_section' => 'header',
            'alpha_zone_branding_weight' => '2',
            'alpha_zone_branding_columns' => '12',
            'alpha_zone_branding_primary' => '',
            'alpha_zone_branding_css' => '',
            'alpha_zone_branding_wrapper_css' => '',
            'alpha_zone_menu_equal_height_container' => '',
            'alpha_zone_menu_wrapper' => '1',
            'alpha_zone_menu_force' => '',
            'alpha_zone_menu_section' => 'header',
            'alpha_zone_menu_weight' => '3',
            'alpha_zone_menu_columns' => '12',
            'alpha_zone_menu_primary' => '',
            'alpha_zone_menu_css' => '',
            'alpha_zone_menu_wrapper_css' => '',
            'alpha_zone_header_equal_height_container' => '',
            'alpha_zone_header_wrapper' => '1',
            'alpha_zone_header_force' => '',
            'alpha_zone_header_section' => 'header',
            'alpha_zone_header_weight' => '4',
            'alpha_zone_header_columns' => '12',
            'alpha_zone_header_primary' => '',
            'alpha_zone_header_css' => '',
            'alpha_zone_header_wrapper_css' => '',
            'alpha_zone_preface_equal_height_container' => '',
            'alpha_zone_preface_wrapper' => '1',
            'alpha_zone_preface_force' => '',
            'alpha_zone_preface_section' => 'content',
            'alpha_zone_preface_weight' => '1',
            'alpha_zone_preface_columns' => '12',
            'alpha_zone_preface_primary' => '',
            'alpha_zone_preface_css' => '',
            'alpha_zone_preface_wrapper_css' => '',
            'alpha_zone_content_equal_height_container' => '',
            'alpha_zone_content_wrapper' => '1',
            'alpha_zone_content_force' => '1',
            'alpha_zone_content_section' => 'content',
            'alpha_zone_content_weight' => '2',
            'alpha_zone_content_columns' => '12',
            'alpha_zone_content_primary' => 'content',
            'alpha_zone_content_css' => '',
            'alpha_zone_content_wrapper_css' => '',
            'alpha_zone_postscript_equal_height_container' => '',
            'alpha_zone_postscript_wrapper' => '1',
            'alpha_zone_postscript_force' => '',
            'alpha_zone_postscript_section' => 'content',
            'alpha_zone_postscript_weight' => '3',
            'alpha_zone_postscript_columns' => '12',
            'alpha_zone_postscript_primary' => '',
            'alpha_zone_postscript_css' => '',
            'alpha_zone_postscript_wrapper_css' => '',
            'alpha_zone_footer_equal_height_container' => '',
            'alpha_zone_footer_wrapper' => '1',
            'alpha_zone_footer_force' => '',
            'alpha_zone_footer_section' => 'footer',
            'alpha_zone_footer_weight' => '1',
            'alpha_zone_footer_columns' => '12',
            'alpha_zone_footer_primary' => '',
            'alpha_zone_footer_css' => '',
            'alpha_zone_footer_wrapper_css' => '',
            'alpha_region_dashboard_sidebar_equal_height_container' => '',
            'alpha_region_dashboard_sidebar_equal_height_element' => '',
            'alpha_region_dashboard_sidebar_force' => '',
            'alpha_region_dashboard_sidebar_zone' => '',
            'alpha_region_dashboard_sidebar_prefix' => '',
            'alpha_region_dashboard_sidebar_columns' => '1',
            'alpha_region_dashboard_sidebar_suffix' => '',
            'alpha_region_dashboard_sidebar_weight' => '-50',
            'alpha_region_dashboard_sidebar_css' => '',
            'alpha_region_dashboard_inactive_equal_height_container' => '',
            'alpha_region_dashboard_inactive_equal_height_element' => '',
            'alpha_region_dashboard_inactive_force' => '',
            'alpha_region_dashboard_inactive_zone' => '',
            'alpha_region_dashboard_inactive_prefix' => '',
            'alpha_region_dashboard_inactive_columns' => '1',
            'alpha_region_dashboard_inactive_suffix' => '',
            'alpha_region_dashboard_inactive_weight' => '-50',
            'alpha_region_dashboard_inactive_css' => '',
            'alpha_region_dashboard_main_equal_height_container' => '',
            'alpha_region_dashboard_main_equal_height_element' => '',
            'alpha_region_dashboard_main_force' => '',
            'alpha_region_dashboard_main_zone' => '',
            'alpha_region_dashboard_main_prefix' => '',
            'alpha_region_dashboard_main_columns' => '1',
            'alpha_region_dashboard_main_suffix' => '',
            'alpha_region_dashboard_main_weight' => '-50',
            'alpha_region_dashboard_main_css' => '',
            'alpha_region_user_first_equal_height_container' => '',
            'alpha_region_user_first_equal_height_element' => '',
            'alpha_region_user_first_force' => '',
            'alpha_region_user_first_zone' => 'user',
            'alpha_region_user_first_prefix' => '',
            'alpha_region_user_first_columns' => '8',
            'alpha_region_user_first_suffix' => '',
            'alpha_region_user_first_weight' => '1',
            'alpha_region_user_first_css' => '',
            'alpha_region_user_second_equal_height_container' => '',
            'alpha_region_user_second_equal_height_element' => '',
            'alpha_region_user_second_force' => '',
            'alpha_region_user_second_zone' => 'user',
            'alpha_region_user_second_prefix' => '',
            'alpha_region_user_second_columns' => '4',
            'alpha_region_user_second_suffix' => '',
            'alpha_region_user_second_weight' => '2',
            'alpha_region_user_second_css' => '',
            'alpha_region_branding_equal_height_container' => '',
            'alpha_region_branding_equal_height_element' => '',
            'alpha_region_branding_force' => '1',
            'alpha_region_branding_zone' => 'branding',
            'alpha_region_branding_prefix' => '',
            'alpha_region_branding_columns' => '12',
            'alpha_region_branding_suffix' => '',
            'alpha_region_branding_weight' => '1',
            'alpha_region_branding_css' => '',
            'alpha_region_menu_equal_height_container' => '',
            'alpha_region_menu_equal_height_element' => '',
            'alpha_region_menu_force' => '1',
            'alpha_region_menu_zone' => 'menu',
            'alpha_region_menu_prefix' => '',
            'alpha_region_menu_columns' => '12',
            'alpha_region_menu_suffix' => '',
            'alpha_region_menu_weight' => '1',
            'alpha_region_menu_css' => '',
            'alpha_region_header_first_equal_height_container' => '',
            'alpha_region_header_first_equal_height_element' => '',
            'alpha_region_header_first_force' => '',
            'alpha_region_header_first_zone' => 'header',
            'alpha_region_header_first_prefix' => '',
            'alpha_region_header_first_columns' => '6',
            'alpha_region_header_first_suffix' => '',
            'alpha_region_header_first_weight' => '1',
            'alpha_region_header_first_css' => '',
            'alpha_region_header_second_equal_height_container' => '',
            'alpha_region_header_second_equal_height_element' => '',
            'alpha_region_header_second_force' => '',
            'alpha_region_header_second_zone' => 'header',
            'alpha_region_header_second_prefix' => '',
            'alpha_region_header_second_columns' => '6',
            'alpha_region_header_second_suffix' => '',
            'alpha_region_header_second_weight' => '2',
            'alpha_region_header_second_css' => '',
            'alpha_region_preface_first_equal_height_container' => '',
            'alpha_region_preface_first_equal_height_element' => '',
            'alpha_region_preface_first_force' => '',
            'alpha_region_preface_first_zone' => 'preface',
            'alpha_region_preface_first_prefix' => '',
            'alpha_region_preface_first_columns' => '4',
            'alpha_region_preface_first_suffix' => '',
            'alpha_region_preface_first_weight' => '1',
            'alpha_region_preface_first_css' => '',
            'alpha_region_preface_second_equal_height_container' => '',
            'alpha_region_preface_second_equal_height_element' => '',
            'alpha_region_preface_second_force' => '',
            'alpha_region_preface_second_zone' => 'preface',
            'alpha_region_preface_second_prefix' => '',
            'alpha_region_preface_second_columns' => '4',
            'alpha_region_preface_second_suffix' => '',
            'alpha_region_preface_second_weight' => '2',
            'alpha_region_preface_second_css' => '',
            'alpha_region_preface_third_equal_height_container' => '',
            'alpha_region_preface_third_equal_height_element' => '',
            'alpha_region_preface_third_force' => '',
            'alpha_region_preface_third_zone' => 'preface',
            'alpha_region_preface_third_prefix' => '',
            'alpha_region_preface_third_columns' => '4',
            'alpha_region_preface_third_suffix' => '',
            'alpha_region_preface_third_weight' => '3',
            'alpha_region_preface_third_css' => '',
            'alpha_region_content_equal_height_container' => '',
            'alpha_region_content_equal_height_element' => '',
            'alpha_region_content_force' => '',
            'alpha_region_content_zone' => 'content',
            'alpha_region_content_prefix' => '',
            'alpha_region_content_columns' => '6',
            'alpha_region_content_suffix' => '',
            'alpha_region_content_weight' => '2',
            'alpha_region_content_css' => '',
            'alpha_region_sidebar_first_equal_height_container' => '',
            'alpha_region_sidebar_first_equal_height_element' => '',
            'alpha_region_sidebar_first_force' => '',
            'alpha_region_sidebar_first_zone' => 'content',
            'alpha_region_sidebar_first_prefix' => '',
            'alpha_region_sidebar_first_columns' => '3',
            'alpha_region_sidebar_first_suffix' => '',
            'alpha_region_sidebar_first_weight' => '1',
            'alpha_region_sidebar_first_css' => '',
            'alpha_region_sidebar_second_equal_height_container' => '',
            'alpha_region_sidebar_second_equal_height_element' => '',
            'alpha_region_sidebar_second_force' => '',
            'alpha_region_sidebar_second_zone' => 'content',
            'alpha_region_sidebar_second_prefix' => '',
            'alpha_region_sidebar_second_columns' => '3',
            'alpha_region_sidebar_second_suffix' => '',
            'alpha_region_sidebar_second_weight' => '3',
            'alpha_region_sidebar_second_css' => '',
            'alpha_region_postscript_first_equal_height_container' => '',
            'alpha_region_postscript_first_equal_height_element' => '',
            'alpha_region_postscript_first_force' => '',
            'alpha_region_postscript_first_zone' => 'postscript',
            'alpha_region_postscript_first_prefix' => '',
            'alpha_region_postscript_first_columns' => '3',
            'alpha_region_postscript_first_suffix' => '',
            'alpha_region_postscript_first_weight' => '1',
            'alpha_region_postscript_first_css' => '',
            'alpha_region_postscript_second_equal_height_container' => '',
            'alpha_region_postscript_second_equal_height_element' => '',
            'alpha_region_postscript_second_force' => '',
            'alpha_region_postscript_second_zone' => 'postscript',
            'alpha_region_postscript_second_prefix' => '',
            'alpha_region_postscript_second_columns' => '3',
            'alpha_region_postscript_second_suffix' => '',
            'alpha_region_postscript_second_weight' => '2',
            'alpha_region_postscript_second_css' => '',
            'alpha_region_postscript_third_equal_height_container' => '',
            'alpha_region_postscript_third_equal_height_element' => '',
            'alpha_region_postscript_third_force' => '',
            'alpha_region_postscript_third_zone' => 'postscript',
            'alpha_region_postscript_third_prefix' => '',
            'alpha_region_postscript_third_columns' => '3',
            'alpha_region_postscript_third_suffix' => '',
            'alpha_region_postscript_third_weight' => '3',
            'alpha_region_postscript_third_css' => '',
            'alpha_region_postscript_fourth_equal_height_container' => '',
            'alpha_region_postscript_fourth_equal_height_element' => '',
            'alpha_region_postscript_fourth_force' => '',
            'alpha_region_postscript_fourth_zone' => 'postscript',
            'alpha_region_postscript_fourth_prefix' => '',
            'alpha_region_postscript_fourth_columns' => '3',
            'alpha_region_postscript_fourth_suffix' => '',
            'alpha_region_postscript_fourth_weight' => '4',
            'alpha_region_postscript_fourth_css' => '',
            'alpha_region_footer_first_equal_height_container' => '',
            'alpha_region_footer_first_equal_height_element' => '',
            'alpha_region_footer_first_force' => '',
            'alpha_region_footer_first_zone' => 'footer',
            'alpha_region_footer_first_prefix' => '',
            'alpha_region_footer_first_columns' => '12',
            'alpha_region_footer_first_suffix' => '',
            'alpha_region_footer_first_weight' => '1',
            'alpha_region_footer_first_css' => '',
            'alpha_region_footer_second_equal_height_container' => '',
            'alpha_region_footer_second_equal_height_element' => '',
            'alpha_region_footer_second_force' => '',
            'alpha_region_footer_second_zone' => 'footer',
            'alpha_region_footer_second_prefix' => '',
            'alpha_region_footer_second_columns' => '12',
            'alpha_region_footer_second_suffix' => '',
            'alpha_region_footer_second_weight' => '2',
            'alpha_region_footer_second_css' => '',
          ),
          'project' => 'omega',
          'datestamp' => '1329681647',
        ),
        'project' => 'omega',
        'version' => '7.x-3.1',
      ),
      'starterkit_omega_xhtml' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/themes/omega/starterkits/omega-xhtml/starterkit_omega_xhtml.info',
        'basename' => 'starterkit_omega_xhtml.info',
        'name' => 'Omega XHTML Starter Kit',
        'info' => 
        array (
          'name' => 'Omega XHTML Starter Kit',
          'description' => 'Default XHTML starterkit for <a href="http://drupal.org/project/omega">Omega</a>. You should not directly edit this starterkit, but make your own copy. Information on this can be found in the <a href="http://himer.us/omega-docs">Omega Documentation</a>.',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'screenshot' => 'screenshot.png',
          'base theme' => 'omega',
          'hidden' => true,
          'starterkit' => true,
          'regions' => 
          array (
            'page_top' => 'Page Top',
            'page_bottom' => 'Page Bottom',
            'content' => 'Content',
            'user_first' => 'User Bar First',
            'user_second' => 'User Bar Second',
            'branding' => 'Branding',
            'menu' => 'Menu',
            'sidebar_first' => 'Sidebar First',
            'sidebar_second' => 'Sidebar Second',
            'header_first' => 'Header First',
            'header_second' => 'Header Second',
            'preface_first' => 'Preface First',
            'preface_second' => 'Preface Second',
            'preface_third' => 'Preface Third',
            'postscript_first' => 'Postscript First',
            'postscript_second' => 'Postscript Second',
            'postscript_third' => 'Postscript Third',
            'postscript_fourth' => 'Postscript Fourth',
            'footer_first' => 'Footer First',
            'footer_second' => 'Footer Second',
          ),
          'zones' => 
          array (
            'user' => 'User',
            'branding' => 'Branding',
            'menu' => 'Menu',
            'header' => 'Header',
            'preface' => 'Preface',
            'content' => 'Content',
            'postscript' => 'Postscript',
            'footer' => 'Footer',
          ),
          'css' => 
          array (
            'global.css' => 
            array (
              'name' => 'Your custom global styles',
              'description' => 'This file holds all the globally active custom CSS of your theme.',
              'options' => 
              array (
                'weight' => '10',
              ),
            ),
          ),
          'settings' => 
          array (
            'alpha_grid' => 'alpha_default',
            'alpha_primary_alpha_default' => 'normal',
            'alpha_responsive' => '1',
            'alpha_layouts_alpha_fluid_primary' => 'normal',
            'alpha_layouts_alpha_fluid_normal_responsive' => '0',
            'alpha_layouts_alpha_fluid_normal_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_primary' => 'normal',
            'alpha_layouts_alpha_default_fluid_responsive' => '0',
            'alpha_layouts_alpha_default_fluid_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_fluid_weight' => '0',
            'alpha_layouts_alpha_default_narrow_responsive' => '1',
            'alpha_layouts_alpha_default_narrow_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_narrow_weight' => '1',
            'alpha_layouts_alpha_default_normal_responsive' => '1',
            'alpha_layouts_alpha_default_normal_media' => 'all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_normal_weight' => '2',
            'alpha_layouts_alpha_default_wide_responsive' => '1',
            'alpha_layouts_alpha_default_wide_media' => 'all and (min-width: 1220px)',
            'alpha_layouts_alpha_default_wide_weight' => '3',
            'alpha_viewport' => '1',
            'alpha_viewport_initial_scale' => '1',
            'alpha_viewport_min_scale' => '1',
            'alpha_viewport_max_scale' => '1',
            'alpha_viewport_user_scaleable' => '',
            'alpha_libraries' => 
            array (
              'omega_formalize' => 'omega_formalize',
              'omega_equalheights' => '',
              'omega_mediaqueries' => 'omega_mediaqueries',
            ),
            'alpha_css' => 
            array (
              'alpha-reset.css' => 'alpha-reset.css',
              'alpha-mobile.css' => 'alpha-mobile.css',
              'alpha-alpha.css' => 'alpha-alpha.css',
              'omega-text.css' => 'omega-text.css',
              'omega-branding.css' => 'omega-branding.css',
              'omega-menu.css' => 'omega-menu.css',
              'omega-forms.css' => 'omega-forms.css',
              'omega-visuals.css' => 'omega-visuals.css',
              'global.css' => 'global.css',
            ),
            'alpha_debug_block_toggle' => '1',
            'alpha_debug_block_active' => '1',
            'alpha_debug_grid_toggle' => '1',
            'alpha_debug_grid_active' => '1',
            'alpha_debug_grid_roles' => 
            array (
              1 => '1',
              2 => '2',
              3 => '3',
            ),
            'alpha_toggle_messages' => '1',
            'alpha_toggle_action_links' => '1',
            'alpha_toggle_tabs' => '1',
            'alpha_toggle_breadcrumb' => '1',
            'alpha_toggle_page_title' => '1',
            'alpha_toggle_feed_icons' => '1',
            'alpha_hidden_title' => '',
            'alpha_hidden_site_name' => '',
            'alpha_hidden_site_slogan' => '',
            'alpha_zone_user_equal_height_container' => '',
            'alpha_zone_user_wrapper' => '1',
            'alpha_zone_user_force' => '',
            'alpha_zone_user_section' => 'header',
            'alpha_zone_user_weight' => '1',
            'alpha_zone_user_columns' => '12',
            'alpha_zone_user_primary' => '',
            'alpha_zone_user_css' => '',
            'alpha_zone_user_wrapper_css' => '',
            'alpha_zone_branding_equal_height_container' => '',
            'alpha_zone_branding_wrapper' => '1',
            'alpha_zone_branding_force' => '',
            'alpha_zone_branding_section' => 'header',
            'alpha_zone_branding_weight' => '2',
            'alpha_zone_branding_columns' => '12',
            'alpha_zone_branding_primary' => '',
            'alpha_zone_branding_css' => '',
            'alpha_zone_branding_wrapper_css' => '',
            'alpha_zone_menu_equal_height_container' => '',
            'alpha_zone_menu_wrapper' => '1',
            'alpha_zone_menu_force' => '',
            'alpha_zone_menu_section' => 'header',
            'alpha_zone_menu_weight' => '3',
            'alpha_zone_menu_columns' => '12',
            'alpha_zone_menu_primary' => '',
            'alpha_zone_menu_css' => '',
            'alpha_zone_menu_wrapper_css' => '',
            'alpha_zone_header_equal_height_container' => '',
            'alpha_zone_header_wrapper' => '1',
            'alpha_zone_header_force' => '',
            'alpha_zone_header_section' => 'header',
            'alpha_zone_header_weight' => '4',
            'alpha_zone_header_columns' => '12',
            'alpha_zone_header_primary' => '',
            'alpha_zone_header_css' => '',
            'alpha_zone_header_wrapper_css' => '',
            'alpha_zone_preface_equal_height_container' => '',
            'alpha_zone_preface_wrapper' => '1',
            'alpha_zone_preface_force' => '',
            'alpha_zone_preface_section' => 'content',
            'alpha_zone_preface_weight' => '1',
            'alpha_zone_preface_columns' => '12',
            'alpha_zone_preface_primary' => '',
            'alpha_zone_preface_css' => '',
            'alpha_zone_preface_wrapper_css' => '',
            'alpha_zone_content_equal_height_container' => '',
            'alpha_zone_content_wrapper' => '1',
            'alpha_zone_content_force' => '1',
            'alpha_zone_content_section' => 'content',
            'alpha_zone_content_weight' => '2',
            'alpha_zone_content_columns' => '12',
            'alpha_zone_content_primary' => 'content',
            'alpha_zone_content_css' => '',
            'alpha_zone_content_wrapper_css' => '',
            'alpha_zone_postscript_equal_height_container' => '',
            'alpha_zone_postscript_wrapper' => '1',
            'alpha_zone_postscript_force' => '',
            'alpha_zone_postscript_section' => 'content',
            'alpha_zone_postscript_weight' => '3',
            'alpha_zone_postscript_columns' => '12',
            'alpha_zone_postscript_primary' => '',
            'alpha_zone_postscript_css' => '',
            'alpha_zone_postscript_wrapper_css' => '',
            'alpha_zone_footer_equal_height_container' => '',
            'alpha_zone_footer_wrapper' => '1',
            'alpha_zone_footer_force' => '',
            'alpha_zone_footer_section' => 'footer',
            'alpha_zone_footer_weight' => '1',
            'alpha_zone_footer_columns' => '12',
            'alpha_zone_footer_primary' => '',
            'alpha_zone_footer_css' => '',
            'alpha_zone_footer_wrapper_css' => '',
            'alpha_region_dashboard_sidebar_equal_height_container' => '',
            'alpha_region_dashboard_sidebar_equal_height_element' => '',
            'alpha_region_dashboard_sidebar_force' => '',
            'alpha_region_dashboard_sidebar_zone' => '',
            'alpha_region_dashboard_sidebar_prefix' => '',
            'alpha_region_dashboard_sidebar_columns' => '1',
            'alpha_region_dashboard_sidebar_suffix' => '',
            'alpha_region_dashboard_sidebar_weight' => '-50',
            'alpha_region_dashboard_sidebar_css' => '',
            'alpha_region_dashboard_inactive_equal_height_container' => '',
            'alpha_region_dashboard_inactive_equal_height_element' => '',
            'alpha_region_dashboard_inactive_force' => '',
            'alpha_region_dashboard_inactive_zone' => '',
            'alpha_region_dashboard_inactive_prefix' => '',
            'alpha_region_dashboard_inactive_columns' => '1',
            'alpha_region_dashboard_inactive_suffix' => '',
            'alpha_region_dashboard_inactive_weight' => '-50',
            'alpha_region_dashboard_inactive_css' => '',
            'alpha_region_dashboard_main_equal_height_container' => '',
            'alpha_region_dashboard_main_equal_height_element' => '',
            'alpha_region_dashboard_main_force' => '',
            'alpha_region_dashboard_main_zone' => '',
            'alpha_region_dashboard_main_prefix' => '',
            'alpha_region_dashboard_main_columns' => '1',
            'alpha_region_dashboard_main_suffix' => '',
            'alpha_region_dashboard_main_weight' => '-50',
            'alpha_region_dashboard_main_css' => '',
            'alpha_region_user_first_equal_height_container' => '',
            'alpha_region_user_first_equal_height_element' => '',
            'alpha_region_user_first_force' => '',
            'alpha_region_user_first_zone' => 'user',
            'alpha_region_user_first_prefix' => '',
            'alpha_region_user_first_columns' => '8',
            'alpha_region_user_first_suffix' => '',
            'alpha_region_user_first_weight' => '1',
            'alpha_region_user_first_css' => '',
            'alpha_region_user_second_equal_height_container' => '',
            'alpha_region_user_second_equal_height_element' => '',
            'alpha_region_user_second_force' => '',
            'alpha_region_user_second_zone' => 'user',
            'alpha_region_user_second_prefix' => '',
            'alpha_region_user_second_columns' => '4',
            'alpha_region_user_second_suffix' => '',
            'alpha_region_user_second_weight' => '2',
            'alpha_region_user_second_css' => '',
            'alpha_region_branding_equal_height_container' => '',
            'alpha_region_branding_equal_height_element' => '',
            'alpha_region_branding_force' => '1',
            'alpha_region_branding_zone' => 'branding',
            'alpha_region_branding_prefix' => '',
            'alpha_region_branding_columns' => '12',
            'alpha_region_branding_suffix' => '',
            'alpha_region_branding_weight' => '1',
            'alpha_region_branding_css' => '',
            'alpha_region_menu_equal_height_container' => '',
            'alpha_region_menu_equal_height_element' => '',
            'alpha_region_menu_force' => '1',
            'alpha_region_menu_zone' => 'menu',
            'alpha_region_menu_prefix' => '',
            'alpha_region_menu_columns' => '12',
            'alpha_region_menu_suffix' => '',
            'alpha_region_menu_weight' => '1',
            'alpha_region_menu_css' => '',
            'alpha_region_header_first_equal_height_container' => '',
            'alpha_region_header_first_equal_height_element' => '',
            'alpha_region_header_first_force' => '',
            'alpha_region_header_first_zone' => 'header',
            'alpha_region_header_first_prefix' => '',
            'alpha_region_header_first_columns' => '6',
            'alpha_region_header_first_suffix' => '',
            'alpha_region_header_first_weight' => '1',
            'alpha_region_header_first_css' => '',
            'alpha_region_header_second_equal_height_container' => '',
            'alpha_region_header_second_equal_height_element' => '',
            'alpha_region_header_second_force' => '',
            'alpha_region_header_second_zone' => 'header',
            'alpha_region_header_second_prefix' => '',
            'alpha_region_header_second_columns' => '6',
            'alpha_region_header_second_suffix' => '',
            'alpha_region_header_second_weight' => '2',
            'alpha_region_header_second_css' => '',
            'alpha_region_preface_first_equal_height_container' => '',
            'alpha_region_preface_first_equal_height_element' => '',
            'alpha_region_preface_first_force' => '',
            'alpha_region_preface_first_zone' => 'preface',
            'alpha_region_preface_first_prefix' => '',
            'alpha_region_preface_first_columns' => '4',
            'alpha_region_preface_first_suffix' => '',
            'alpha_region_preface_first_weight' => '1',
            'alpha_region_preface_first_css' => '',
            'alpha_region_preface_second_equal_height_container' => '',
            'alpha_region_preface_second_equal_height_element' => '',
            'alpha_region_preface_second_force' => '',
            'alpha_region_preface_second_zone' => 'preface',
            'alpha_region_preface_second_prefix' => '',
            'alpha_region_preface_second_columns' => '4',
            'alpha_region_preface_second_suffix' => '',
            'alpha_region_preface_second_weight' => '2',
            'alpha_region_preface_second_css' => '',
            'alpha_region_preface_third_equal_height_container' => '',
            'alpha_region_preface_third_equal_height_element' => '',
            'alpha_region_preface_third_force' => '',
            'alpha_region_preface_third_zone' => 'preface',
            'alpha_region_preface_third_prefix' => '',
            'alpha_region_preface_third_columns' => '4',
            'alpha_region_preface_third_suffix' => '',
            'alpha_region_preface_third_weight' => '3',
            'alpha_region_preface_third_css' => '',
            'alpha_region_content_equal_height_container' => '',
            'alpha_region_content_equal_height_element' => '',
            'alpha_region_content_force' => '',
            'alpha_region_content_zone' => 'content',
            'alpha_region_content_prefix' => '',
            'alpha_region_content_columns' => '6',
            'alpha_region_content_suffix' => '',
            'alpha_region_content_weight' => '2',
            'alpha_region_content_css' => '',
            'alpha_region_sidebar_first_equal_height_container' => '',
            'alpha_region_sidebar_first_equal_height_element' => '',
            'alpha_region_sidebar_first_force' => '',
            'alpha_region_sidebar_first_zone' => 'content',
            'alpha_region_sidebar_first_prefix' => '',
            'alpha_region_sidebar_first_columns' => '3',
            'alpha_region_sidebar_first_suffix' => '',
            'alpha_region_sidebar_first_weight' => '1',
            'alpha_region_sidebar_first_css' => '',
            'alpha_region_sidebar_second_equal_height_container' => '',
            'alpha_region_sidebar_second_equal_height_element' => '',
            'alpha_region_sidebar_second_force' => '',
            'alpha_region_sidebar_second_zone' => 'content',
            'alpha_region_sidebar_second_prefix' => '',
            'alpha_region_sidebar_second_columns' => '3',
            'alpha_region_sidebar_second_suffix' => '',
            'alpha_region_sidebar_second_weight' => '3',
            'alpha_region_sidebar_second_css' => '',
            'alpha_region_postscript_first_equal_height_container' => '',
            'alpha_region_postscript_first_equal_height_element' => '',
            'alpha_region_postscript_first_force' => '',
            'alpha_region_postscript_first_zone' => 'postscript',
            'alpha_region_postscript_first_prefix' => '',
            'alpha_region_postscript_first_columns' => '3',
            'alpha_region_postscript_first_suffix' => '',
            'alpha_region_postscript_first_weight' => '1',
            'alpha_region_postscript_first_css' => '',
            'alpha_region_postscript_second_equal_height_container' => '',
            'alpha_region_postscript_second_equal_height_element' => '',
            'alpha_region_postscript_second_force' => '',
            'alpha_region_postscript_second_zone' => 'postscript',
            'alpha_region_postscript_second_prefix' => '',
            'alpha_region_postscript_second_columns' => '3',
            'alpha_region_postscript_second_suffix' => '',
            'alpha_region_postscript_second_weight' => '2',
            'alpha_region_postscript_second_css' => '',
            'alpha_region_postscript_third_equal_height_container' => '',
            'alpha_region_postscript_third_equal_height_element' => '',
            'alpha_region_postscript_third_force' => '',
            'alpha_region_postscript_third_zone' => 'postscript',
            'alpha_region_postscript_third_prefix' => '',
            'alpha_region_postscript_third_columns' => '3',
            'alpha_region_postscript_third_suffix' => '',
            'alpha_region_postscript_third_weight' => '3',
            'alpha_region_postscript_third_css' => '',
            'alpha_region_postscript_fourth_equal_height_container' => '',
            'alpha_region_postscript_fourth_equal_height_element' => '',
            'alpha_region_postscript_fourth_force' => '',
            'alpha_region_postscript_fourth_zone' => 'postscript',
            'alpha_region_postscript_fourth_prefix' => '',
            'alpha_region_postscript_fourth_columns' => '3',
            'alpha_region_postscript_fourth_suffix' => '',
            'alpha_region_postscript_fourth_weight' => '4',
            'alpha_region_postscript_fourth_css' => '',
            'alpha_region_footer_first_equal_height_container' => '',
            'alpha_region_footer_first_equal_height_element' => '',
            'alpha_region_footer_first_force' => '',
            'alpha_region_footer_first_zone' => 'footer',
            'alpha_region_footer_first_prefix' => '',
            'alpha_region_footer_first_columns' => '12',
            'alpha_region_footer_first_suffix' => '',
            'alpha_region_footer_first_weight' => '1',
            'alpha_region_footer_first_css' => '',
            'alpha_region_footer_second_equal_height_container' => '',
            'alpha_region_footer_second_equal_height_element' => '',
            'alpha_region_footer_second_force' => '',
            'alpha_region_footer_second_zone' => 'footer',
            'alpha_region_footer_second_prefix' => '',
            'alpha_region_footer_second_columns' => '12',
            'alpha_region_footer_second_suffix' => '',
            'alpha_region_footer_second_weight' => '2',
            'alpha_region_footer_second_css' => '',
          ),
          'version' => '7.x-3.1',
          'project' => 'omega',
          'datestamp' => '1329681647',
        ),
        'project' => 'omega',
        'version' => '7.x-3.1',
      ),
      'starterkit_omega_html5' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/themes/omega/starterkits/omega-html5/starterkit_omega_html5.info',
        'basename' => 'starterkit_omega_html5.info',
        'name' => 'Omega HTML5 Starterkit',
        'info' => 
        array (
          'name' => 'Omega HTML5 Starterkit',
          'description' => 'Default starterkit for <a href="http://drupal.org/project/omega">Omega</a>. You should not directly edit this starterkit, but make your own copy. Information on this can be found in the <a href="http://himer.us/omega-docs">Omega Documentation</a>',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'screenshot' => 'screenshot.png',
          'base theme' => 'omega',
          'hidden' => true,
          'starterkit' => true,
          'regions' => 
          array (
            'page_top' => 'Page Top',
            'page_bottom' => 'Page Bottom',
            'content' => 'Content',
            'user_first' => 'User Bar First',
            'user_second' => 'User Bar Second',
            'branding' => 'Branding',
            'menu' => 'Menu',
            'sidebar_first' => 'Sidebar First',
            'sidebar_second' => 'Sidebar Second',
            'header_first' => 'Header First',
            'header_second' => 'Header Second',
            'preface_first' => 'Preface First',
            'preface_second' => 'Preface Second',
            'preface_third' => 'Preface Third',
            'postscript_first' => 'Postscript First',
            'postscript_second' => 'Postscript Second',
            'postscript_third' => 'Postscript Third',
            'postscript_fourth' => 'Postscript Fourth',
            'footer_first' => 'Footer First',
            'footer_second' => 'Footer Second',
          ),
          'zones' => 
          array (
            'user' => 'User',
            'branding' => 'Branding',
            'menu' => 'Menu',
            'header' => 'Header',
            'preface' => 'Preface',
            'content' => 'Content',
            'postscript' => 'Postscript',
            'footer' => 'Footer',
          ),
          'css' => 
          array (
            'global.css' => 
            array (
              'name' => 'Your custom global styles',
              'description' => 'This file holds all the globally active custom CSS of your theme.',
              'options' => 
              array (
                'weight' => '10',
              ),
            ),
          ),
          'settings' => 
          array (
            'alpha_grid' => 'alpha_default',
            'alpha_primary_alpha_default' => 'normal',
            'alpha_responsive' => '1',
            'alpha_layouts_alpha_fluid_primary' => 'normal',
            'alpha_layouts_alpha_fluid_normal_responsive' => '0',
            'alpha_layouts_alpha_fluid_normal_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_primary' => 'normal',
            'alpha_layouts_alpha_default_fluid_responsive' => '0',
            'alpha_layouts_alpha_default_fluid_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_fluid_weight' => '0',
            'alpha_layouts_alpha_default_narrow_responsive' => '1',
            'alpha_layouts_alpha_default_narrow_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_narrow_weight' => '1',
            'alpha_layouts_alpha_default_normal_responsive' => '1',
            'alpha_layouts_alpha_default_normal_media' => 'all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_normal_weight' => '2',
            'alpha_layouts_alpha_default_wide_responsive' => '1',
            'alpha_layouts_alpha_default_wide_media' => 'all and (min-width: 1220px)',
            'alpha_layouts_alpha_default_wide_weight' => '3',
            'alpha_viewport' => '1',
            'alpha_viewport_initial_scale' => '1',
            'alpha_viewport_min_scale' => '1',
            'alpha_viewport_max_scale' => '1',
            'alpha_viewport_user_scaleable' => '',
            'alpha_libraries' => 
            array (
              'omega_formalize' => 'omega_formalize',
              'omega_equalheights' => '',
              'omega_mediaqueries' => 'omega_mediaqueries',
            ),
            'alpha_css' => 
            array (
              'alpha-reset.css' => 'alpha-reset.css',
              'alpha-mobile.css' => 'alpha-mobile.css',
              'alpha-alpha.css' => 'alpha-alpha.css',
              'omega-text.css' => 'omega-text.css',
              'omega-branding.css' => 'omega-branding.css',
              'omega-menu.css' => 'omega-menu.css',
              'omega-forms.css' => 'omega-forms.css',
              'omega-visuals.css' => 'omega-visuals.css',
              'global.css' => 'global.css',
            ),
            'alpha_debug_block_toggle' => '1',
            'alpha_debug_block_active' => '1',
            'alpha_debug_grid_toggle' => '1',
            'alpha_debug_grid_active' => '1',
            'alpha_debug_grid_roles' => 
            array (
              1 => '1',
              2 => '2',
              3 => '3',
            ),
            'alpha_toggle_messages' => '1',
            'alpha_toggle_action_links' => '1',
            'alpha_toggle_tabs' => '1',
            'alpha_toggle_breadcrumb' => '1',
            'alpha_toggle_page_title' => '1',
            'alpha_toggle_feed_icons' => '1',
            'alpha_hidden_title' => '',
            'alpha_hidden_site_name' => '',
            'alpha_hidden_site_slogan' => '',
            'alpha_zone_user_equal_height_container' => '',
            'alpha_zone_user_wrapper' => '1',
            'alpha_zone_user_force' => '',
            'alpha_zone_user_section' => 'header',
            'alpha_zone_user_weight' => '1',
            'alpha_zone_user_columns' => '12',
            'alpha_zone_user_primary' => '',
            'alpha_zone_user_css' => '',
            'alpha_zone_user_wrapper_css' => '',
            'alpha_zone_branding_equal_height_container' => '',
            'alpha_zone_branding_wrapper' => '1',
            'alpha_zone_branding_force' => '',
            'alpha_zone_branding_section' => 'header',
            'alpha_zone_branding_weight' => '2',
            'alpha_zone_branding_columns' => '12',
            'alpha_zone_branding_primary' => '',
            'alpha_zone_branding_css' => '',
            'alpha_zone_branding_wrapper_css' => '',
            'alpha_zone_menu_equal_height_container' => '',
            'alpha_zone_menu_wrapper' => '1',
            'alpha_zone_menu_force' => '',
            'alpha_zone_menu_section' => 'header',
            'alpha_zone_menu_weight' => '3',
            'alpha_zone_menu_columns' => '12',
            'alpha_zone_menu_primary' => '',
            'alpha_zone_menu_css' => '',
            'alpha_zone_menu_wrapper_css' => '',
            'alpha_zone_header_equal_height_container' => '',
            'alpha_zone_header_wrapper' => '1',
            'alpha_zone_header_force' => '',
            'alpha_zone_header_section' => 'header',
            'alpha_zone_header_weight' => '4',
            'alpha_zone_header_columns' => '12',
            'alpha_zone_header_primary' => '',
            'alpha_zone_header_css' => '',
            'alpha_zone_header_wrapper_css' => '',
            'alpha_zone_preface_equal_height_container' => '',
            'alpha_zone_preface_wrapper' => '1',
            'alpha_zone_preface_force' => '',
            'alpha_zone_preface_section' => 'content',
            'alpha_zone_preface_weight' => '1',
            'alpha_zone_preface_columns' => '12',
            'alpha_zone_preface_primary' => '',
            'alpha_zone_preface_css' => '',
            'alpha_zone_preface_wrapper_css' => '',
            'alpha_zone_content_equal_height_container' => '',
            'alpha_zone_content_wrapper' => '1',
            'alpha_zone_content_force' => '1',
            'alpha_zone_content_section' => 'content',
            'alpha_zone_content_weight' => '2',
            'alpha_zone_content_columns' => '12',
            'alpha_zone_content_primary' => 'content',
            'alpha_zone_content_css' => '',
            'alpha_zone_content_wrapper_css' => '',
            'alpha_zone_postscript_equal_height_container' => '',
            'alpha_zone_postscript_wrapper' => '1',
            'alpha_zone_postscript_force' => '',
            'alpha_zone_postscript_section' => 'content',
            'alpha_zone_postscript_weight' => '3',
            'alpha_zone_postscript_columns' => '12',
            'alpha_zone_postscript_primary' => '',
            'alpha_zone_postscript_css' => '',
            'alpha_zone_postscript_wrapper_css' => '',
            'alpha_zone_footer_equal_height_container' => '',
            'alpha_zone_footer_wrapper' => '1',
            'alpha_zone_footer_force' => '',
            'alpha_zone_footer_section' => 'footer',
            'alpha_zone_footer_weight' => '1',
            'alpha_zone_footer_columns' => '12',
            'alpha_zone_footer_primary' => '',
            'alpha_zone_footer_css' => '',
            'alpha_zone_footer_wrapper_css' => '',
            'alpha_region_dashboard_sidebar_equal_height_container' => '',
            'alpha_region_dashboard_sidebar_equal_height_element' => '',
            'alpha_region_dashboard_sidebar_force' => '',
            'alpha_region_dashboard_sidebar_zone' => '',
            'alpha_region_dashboard_sidebar_prefix' => '',
            'alpha_region_dashboard_sidebar_columns' => '1',
            'alpha_region_dashboard_sidebar_suffix' => '',
            'alpha_region_dashboard_sidebar_weight' => '-50',
            'alpha_region_dashboard_sidebar_css' => '',
            'alpha_region_dashboard_inactive_equal_height_container' => '',
            'alpha_region_dashboard_inactive_equal_height_element' => '',
            'alpha_region_dashboard_inactive_force' => '',
            'alpha_region_dashboard_inactive_zone' => '',
            'alpha_region_dashboard_inactive_prefix' => '',
            'alpha_region_dashboard_inactive_columns' => '1',
            'alpha_region_dashboard_inactive_suffix' => '',
            'alpha_region_dashboard_inactive_weight' => '-50',
            'alpha_region_dashboard_inactive_css' => '',
            'alpha_region_dashboard_main_equal_height_container' => '',
            'alpha_region_dashboard_main_equal_height_element' => '',
            'alpha_region_dashboard_main_force' => '',
            'alpha_region_dashboard_main_zone' => '',
            'alpha_region_dashboard_main_prefix' => '',
            'alpha_region_dashboard_main_columns' => '1',
            'alpha_region_dashboard_main_suffix' => '',
            'alpha_region_dashboard_main_weight' => '-50',
            'alpha_region_dashboard_main_css' => '',
            'alpha_region_user_first_equal_height_container' => '',
            'alpha_region_user_first_equal_height_element' => '',
            'alpha_region_user_first_force' => '',
            'alpha_region_user_first_zone' => 'user',
            'alpha_region_user_first_prefix' => '',
            'alpha_region_user_first_columns' => '8',
            'alpha_region_user_first_suffix' => '',
            'alpha_region_user_first_weight' => '1',
            'alpha_region_user_first_css' => '',
            'alpha_region_user_second_equal_height_container' => '',
            'alpha_region_user_second_equal_height_element' => '',
            'alpha_region_user_second_force' => '',
            'alpha_region_user_second_zone' => 'user',
            'alpha_region_user_second_prefix' => '',
            'alpha_region_user_second_columns' => '4',
            'alpha_region_user_second_suffix' => '',
            'alpha_region_user_second_weight' => '2',
            'alpha_region_user_second_css' => '',
            'alpha_region_branding_equal_height_container' => '',
            'alpha_region_branding_equal_height_element' => '',
            'alpha_region_branding_force' => '1',
            'alpha_region_branding_zone' => 'branding',
            'alpha_region_branding_prefix' => '',
            'alpha_region_branding_columns' => '12',
            'alpha_region_branding_suffix' => '',
            'alpha_region_branding_weight' => '1',
            'alpha_region_branding_css' => '',
            'alpha_region_menu_equal_height_container' => '',
            'alpha_region_menu_equal_height_element' => '',
            'alpha_region_menu_force' => '1',
            'alpha_region_menu_zone' => 'menu',
            'alpha_region_menu_prefix' => '',
            'alpha_region_menu_columns' => '12',
            'alpha_region_menu_suffix' => '',
            'alpha_region_menu_weight' => '1',
            'alpha_region_menu_css' => '',
            'alpha_region_header_first_equal_height_container' => '',
            'alpha_region_header_first_equal_height_element' => '',
            'alpha_region_header_first_force' => '',
            'alpha_region_header_first_zone' => 'header',
            'alpha_region_header_first_prefix' => '',
            'alpha_region_header_first_columns' => '6',
            'alpha_region_header_first_suffix' => '',
            'alpha_region_header_first_weight' => '1',
            'alpha_region_header_first_css' => '',
            'alpha_region_header_second_equal_height_container' => '',
            'alpha_region_header_second_equal_height_element' => '',
            'alpha_region_header_second_force' => '',
            'alpha_region_header_second_zone' => 'header',
            'alpha_region_header_second_prefix' => '',
            'alpha_region_header_second_columns' => '6',
            'alpha_region_header_second_suffix' => '',
            'alpha_region_header_second_weight' => '2',
            'alpha_region_header_second_css' => '',
            'alpha_region_preface_first_equal_height_container' => '',
            'alpha_region_preface_first_equal_height_element' => '',
            'alpha_region_preface_first_force' => '',
            'alpha_region_preface_first_zone' => 'preface',
            'alpha_region_preface_first_prefix' => '',
            'alpha_region_preface_first_columns' => '4',
            'alpha_region_preface_first_suffix' => '',
            'alpha_region_preface_first_weight' => '1',
            'alpha_region_preface_first_css' => '',
            'alpha_region_preface_second_equal_height_container' => '',
            'alpha_region_preface_second_equal_height_element' => '',
            'alpha_region_preface_second_force' => '',
            'alpha_region_preface_second_zone' => 'preface',
            'alpha_region_preface_second_prefix' => '',
            'alpha_region_preface_second_columns' => '4',
            'alpha_region_preface_second_suffix' => '',
            'alpha_region_preface_second_weight' => '2',
            'alpha_region_preface_second_css' => '',
            'alpha_region_preface_third_equal_height_container' => '',
            'alpha_region_preface_third_equal_height_element' => '',
            'alpha_region_preface_third_force' => '',
            'alpha_region_preface_third_zone' => 'preface',
            'alpha_region_preface_third_prefix' => '',
            'alpha_region_preface_third_columns' => '4',
            'alpha_region_preface_third_suffix' => '',
            'alpha_region_preface_third_weight' => '3',
            'alpha_region_preface_third_css' => '',
            'alpha_region_content_equal_height_container' => '',
            'alpha_region_content_equal_height_element' => '',
            'alpha_region_content_force' => '',
            'alpha_region_content_zone' => 'content',
            'alpha_region_content_prefix' => '',
            'alpha_region_content_columns' => '6',
            'alpha_region_content_suffix' => '',
            'alpha_region_content_weight' => '2',
            'alpha_region_content_css' => '',
            'alpha_region_sidebar_first_equal_height_container' => '',
            'alpha_region_sidebar_first_equal_height_element' => '',
            'alpha_region_sidebar_first_force' => '',
            'alpha_region_sidebar_first_zone' => 'content',
            'alpha_region_sidebar_first_prefix' => '',
            'alpha_region_sidebar_first_columns' => '3',
            'alpha_region_sidebar_first_suffix' => '',
            'alpha_region_sidebar_first_weight' => '1',
            'alpha_region_sidebar_first_css' => '',
            'alpha_region_sidebar_second_equal_height_container' => '',
            'alpha_region_sidebar_second_equal_height_element' => '',
            'alpha_region_sidebar_second_force' => '',
            'alpha_region_sidebar_second_zone' => 'content',
            'alpha_region_sidebar_second_prefix' => '',
            'alpha_region_sidebar_second_columns' => '3',
            'alpha_region_sidebar_second_suffix' => '',
            'alpha_region_sidebar_second_weight' => '3',
            'alpha_region_sidebar_second_css' => '',
            'alpha_region_postscript_first_equal_height_container' => '',
            'alpha_region_postscript_first_equal_height_element' => '',
            'alpha_region_postscript_first_force' => '',
            'alpha_region_postscript_first_zone' => 'postscript',
            'alpha_region_postscript_first_prefix' => '',
            'alpha_region_postscript_first_columns' => '3',
            'alpha_region_postscript_first_suffix' => '',
            'alpha_region_postscript_first_weight' => '1',
            'alpha_region_postscript_first_css' => '',
            'alpha_region_postscript_second_equal_height_container' => '',
            'alpha_region_postscript_second_equal_height_element' => '',
            'alpha_region_postscript_second_force' => '',
            'alpha_region_postscript_second_zone' => 'postscript',
            'alpha_region_postscript_second_prefix' => '',
            'alpha_region_postscript_second_columns' => '3',
            'alpha_region_postscript_second_suffix' => '',
            'alpha_region_postscript_second_weight' => '2',
            'alpha_region_postscript_second_css' => '',
            'alpha_region_postscript_third_equal_height_container' => '',
            'alpha_region_postscript_third_equal_height_element' => '',
            'alpha_region_postscript_third_force' => '',
            'alpha_region_postscript_third_zone' => 'postscript',
            'alpha_region_postscript_third_prefix' => '',
            'alpha_region_postscript_third_columns' => '3',
            'alpha_region_postscript_third_suffix' => '',
            'alpha_region_postscript_third_weight' => '3',
            'alpha_region_postscript_third_css' => '',
            'alpha_region_postscript_fourth_equal_height_container' => '',
            'alpha_region_postscript_fourth_equal_height_element' => '',
            'alpha_region_postscript_fourth_force' => '',
            'alpha_region_postscript_fourth_zone' => 'postscript',
            'alpha_region_postscript_fourth_prefix' => '',
            'alpha_region_postscript_fourth_columns' => '3',
            'alpha_region_postscript_fourth_suffix' => '',
            'alpha_region_postscript_fourth_weight' => '4',
            'alpha_region_postscript_fourth_css' => '',
            'alpha_region_footer_first_equal_height_container' => '',
            'alpha_region_footer_first_equal_height_element' => '',
            'alpha_region_footer_first_force' => '',
            'alpha_region_footer_first_zone' => 'footer',
            'alpha_region_footer_first_prefix' => '',
            'alpha_region_footer_first_columns' => '12',
            'alpha_region_footer_first_suffix' => '',
            'alpha_region_footer_first_weight' => '1',
            'alpha_region_footer_first_css' => '',
            'alpha_region_footer_second_equal_height_container' => '',
            'alpha_region_footer_second_equal_height_element' => '',
            'alpha_region_footer_second_force' => '',
            'alpha_region_footer_second_zone' => 'footer',
            'alpha_region_footer_second_prefix' => '',
            'alpha_region_footer_second_columns' => '12',
            'alpha_region_footer_second_suffix' => '',
            'alpha_region_footer_second_weight' => '2',
            'alpha_region_footer_second_css' => '',
          ),
          'version' => '7.x-3.1',
          'project' => 'omega',
          'datestamp' => '1329681647',
        ),
        'project' => 'omega',
        'version' => '7.x-3.1',
      ),
      'starterkit_alpha_xhtml' => 
      array (
        'filename' => '/var/aegir/platforms/gdp.dev1.a/sites/all/themes/omega/starterkits/alpha-xhtml/starterkit_alpha_xhtml.info',
        'basename' => 'starterkit_alpha_xhtml.info',
        'name' => 'Alpha XHTML Starterkit',
        'info' => 
        array (
          'name' => 'Alpha XHTML Starterkit',
          'description' => 'Default starterkit for <a href="http://drupal.org/project/omega">Alpha</a>. You should not directly edit this starterkit, but make your own copy. Information on this can be found in the <a href="http://himer.us/omega-docs">Omega Documentation</a>',
          'core' => '7.x',
          'engine' => 'phptemplate',
          'screenshot' => 'screenshot.png',
          'base theme' => 'alpha',
          'hidden' => true,
          'starterkit' => true,
          'regions' => 
          array (
            'page_top' => 'Page Top',
            'page_bottom' => 'Page Bottom',
            'content' => 'Content',
            'header' => 'Header',
            'footer' => 'Footer',
            'sidebar_first' => 'Sidebar First',
            'sidebar_second' => 'Sidebar Second',
          ),
          'zones' => 
          array (
            'content' => 'Content',
            'header' => 'Header',
            'footer' => 'Footer',
          ),
          'css' => 
          array (
            'global.css' => 
            array (
              'name' => 'Your custom global styles',
              'description' => 'This file holds all the globally active custom CSS of your theme.',
              'options' => 
              array (
                'weight' => '10',
              ),
            ),
          ),
          'settings' => 
          array (
            'alpha_grid' => 'alpha_default',
            'alpha_primary_alpha_default' => 'normal',
            'alpha_responsive' => '1',
            'alpha_layouts_alpha_fluid_primary' => 'normal',
            'alpha_layouts_alpha_fluid_normal_responsive' => '0',
            'alpha_layouts_alpha_fluid_normal_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_primary' => 'normal',
            'alpha_layouts_alpha_default_fluid_responsive' => '0',
            'alpha_layouts_alpha_default_fluid_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_fluid_weight' => '0',
            'alpha_layouts_alpha_default_narrow_responsive' => '1',
            'alpha_layouts_alpha_default_narrow_media' => 'all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_narrow_weight' => '1',
            'alpha_layouts_alpha_default_normal_responsive' => '1',
            'alpha_layouts_alpha_default_normal_media' => 'all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)',
            'alpha_layouts_alpha_default_normal_weight' => '2',
            'alpha_layouts_alpha_default_wide_responsive' => '1',
            'alpha_layouts_alpha_default_wide_media' => 'all and (min-width: 1220px)',
            'alpha_layouts_alpha_default_wide_weight' => '3',
            'alpha_viewport' => '1',
            'alpha_viewport_initial_scale' => '1',
            'alpha_viewport_min_scale' => '1',
            'alpha_viewport_max_scale' => '1',
            'alpha_viewport_user_scaleable' => '',
            'alpha_css' => 
            array (
              'alpha-reset.css' => 'alpha-reset.css',
              'alpha-alpha.css' => 'alpha-alpha.css',
              'alpha-mobile.css' => 'alpha-mobile.css',
              'global.css' => 'global.css',
            ),
            'alpha_debug_block_toggle' => '1',
            'alpha_debug_block_active' => '1',
            'alpha_debug_grid_toggle' => '1',
            'alpha_debug_grid_active' => '1',
            'alpha_debug_grid_roles' => 
            array (
              1 => '1',
              2 => '2',
              3 => '3',
            ),
            'alpha_toggle_messages' => '1',
            'alpha_toggle_action_links' => '1',
            'alpha_toggle_tabs' => '1',
            'alpha_toggle_breadcrumb' => '1',
            'alpha_toggle_page_title' => '1',
            'alpha_toggle_feed_icons' => '1',
            'alpha_hidden_title' => '',
            'alpha_hidden_site_name' => '',
            'alpha_hidden_site_slogan' => '',
            'alpha_zone_header_wrapper' => '',
            'alpha_zone_header_force' => '',
            'alpha_zone_header_section' => 'header',
            'alpha_zone_header_weight' => '',
            'alpha_zone_header_columns' => '12',
            'alpha_zone_header_primary' => '',
            'alpha_zone_header_order' => '0',
            'alpha_zone_header_css' => '',
            'alpha_zone_header_wrapper_css' => '',
            'alpha_zone_content_wrapper' => '',
            'alpha_zone_content_force' => '',
            'alpha_zone_content_section' => 'content',
            'alpha_zone_content_weight' => '',
            'alpha_zone_content_columns' => '12',
            'alpha_zone_content_primary' => '',
            'alpha_zone_content_order' => '0',
            'alpha_zone_content_css' => '',
            'alpha_zone_content_wrapper_css' => '',
            'alpha_zone_footer_wrapper' => '',
            'alpha_zone_footer_force' => '',
            'alpha_zone_footer_section' => 'footer',
            'alpha_zone_footer_weight' => '',
            'alpha_zone_footer_columns' => '12',
            'alpha_zone_footer_primary' => '',
            'alpha_zone_footer_order' => '0',
            'alpha_zone_footer_css' => '',
            'alpha_zone_footer_wrapper_css' => '',
            'alpha_region_dashboard_inactive_force' => '',
            'alpha_region_dashboard_inactive_zone' => '',
            'alpha_region_dashboard_inactive_prefix' => '',
            'alpha_region_dashboard_inactive_columns' => '',
            'alpha_region_dashboard_inactive_suffix' => '',
            'alpha_region_dashboard_inactive_weight' => '',
            'alpha_region_dashboard_inactive_position' => '',
            'alpha_region_dashboard_inactive_css' => '',
            'alpha_region_dashboard_sidebar_force' => '',
            'alpha_region_dashboard_sidebar_zone' => '',
            'alpha_region_dashboard_sidebar_prefix' => '',
            'alpha_region_dashboard_sidebar_columns' => '',
            'alpha_region_dashboard_sidebar_suffix' => '',
            'alpha_region_dashboard_sidebar_weight' => '',
            'alpha_region_dashboard_sidebar_position' => '',
            'alpha_region_dashboard_sidebar_css' => '',
            'alpha_region_dashboard_main_force' => '',
            'alpha_region_dashboard_main_zone' => '',
            'alpha_region_dashboard_main_prefix' => '',
            'alpha_region_dashboard_main_columns' => '',
            'alpha_region_dashboard_main_suffix' => '',
            'alpha_region_dashboard_main_weight' => '',
            'alpha_region_dashboard_main_position' => '',
            'alpha_region_dashboard_main_css' => '',
            'alpha_region_header_force' => '',
            'alpha_region_header_zone' => 'header',
            'alpha_region_header_prefix' => '',
            'alpha_region_header_columns' => '12',
            'alpha_region_header_suffix' => '',
            'alpha_region_header_weight' => '',
            'alpha_region_header_position' => '1',
            'alpha_region_header_css' => '',
            'alpha_region_content_force' => '',
            'alpha_region_content_zone' => 'content',
            'alpha_region_content_prefix' => '',
            'alpha_region_content_columns' => '6',
            'alpha_region_content_suffix' => '',
            'alpha_region_content_weight' => '1',
            'alpha_region_content_position' => '2',
            'alpha_region_content_css' => '',
            'alpha_region_sidebar_first_force' => '',
            'alpha_region_sidebar_first_zone' => 'content',
            'alpha_region_sidebar_first_prefix' => '',
            'alpha_region_sidebar_first_columns' => '3',
            'alpha_region_sidebar_first_suffix' => '',
            'alpha_region_sidebar_first_weight' => '2',
            'alpha_region_sidebar_first_position' => '1',
            'alpha_region_sidebar_first_css' => '',
            'alpha_region_sidebar_second_force' => '',
            'alpha_region_sidebar_second_zone' => 'content',
            'alpha_region_sidebar_second_prefix' => '',
            'alpha_region_sidebar_second_columns' => '3',
            'alpha_region_sidebar_second_suffix' => '',
            'alpha_region_sidebar_second_weight' => '3',
            'alpha_region_sidebar_second_position' => '3',
            'alpha_region_sidebar_second_css' => '',
            'alpha_region_footer_force' => '',
            'alpha_region_footer_zone' => 'footer',
            'alpha_region_footer_prefix' => '',
            'alpha_region_footer_columns' => '12',
            'alpha_region_footer_suffix' => '',
            'alpha_region_footer_weight' => '1',
            'alpha_region_footer_css' => '',
          ),
          'version' => '7.x-3.1',
          'project' => 'omega',
          'datestamp' => '1329681647',
        ),
        'project' => 'omega',
        'version' => '7.x-3.1',
      ),
    ),
  ),
  'profiles' => 
  array (
    'minimal' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
    'standard' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
    'testing' => 
    array (
      'modules' => 
      array (
        'drupal_system_listing_incompatible_test' => 
        array (
          'filename' => '/var/aegir/platforms/gdp.dev1.a/profiles/testing/modules/drupal_system_listing_incompatible_test/drupal_system_listing_incompatible_test.module',
          'basename' => 'drupal_system_listing_incompatible_test.module',
          'name' => 'drupal_system_listing_incompatible_test',
          'info' => 
          array (
            'name' => 'Drupal system listing incompatible test',
            'description' => 'Support module for testing the drupal_system_listing function.',
            'package' => 'Testing',
            'version' => '7.21',
            'core' => '6.x',
            'hidden' => true,
            'project' => 'drupal',
            'datestamp' => '1362616996',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'project' => 'drupal',
          'version' => '7.21',
        ),
        'drupal_system_listing_compatible_test' => 
        array (
          'filename' => '/var/aegir/platforms/gdp.dev1.a/profiles/testing/modules/drupal_system_listing_compatible_test/drupal_system_listing_compatible_test.module',
          'basename' => 'drupal_system_listing_compatible_test.module',
          'name' => 'drupal_system_listing_compatible_test',
          'info' => 
          array (
            'name' => 'Drupal system listing compatible test',
            'description' => 'Support module for testing the drupal_system_listing function.',
            'package' => 'Testing',
            'version' => '7.21',
            'core' => '7.x',
            'hidden' => true,
            'files' => 
            array (
              0 => 'drupal_system_listing_compatible_test.test',
            ),
            'project' => 'drupal',
            'datestamp' => '1362616996',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'project' => 'drupal',
          'version' => '7.21',
        ),
      ),
      'themes' => 
      array (
      ),
    ),
  ),
);