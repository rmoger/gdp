<?php
/**
 * @file
 * country.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function country_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_country';
  $strongarm->value = 0;
  $export['comment_anonymous_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_country';
  $strongarm->value = '1';
  $export['comment_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_country';
  $strongarm->value = 1;
  $export['comment_default_mode_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_country';
  $strongarm->value = '50';
  $export['comment_default_per_page_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_country';
  $strongarm->value = 1;
  $export['comment_form_location_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_country';
  $strongarm->value = '1';
  $export['comment_preview_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_country';
  $strongarm->value = 1;
  $export['comment_subject_field_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__country';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'gdp_admin' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'additional_settings' => array(
          'weight' => '99',
        ),
        'revision_information' => array(
          'weight' => '20',
        ),
        'revision' => array(
          'weight' => '0',
        ),
        'log' => array(
          'weight' => '1',
        ),
        'author' => array(
          'weight' => '90',
        ),
        'name' => array(
          'weight' => '-1',
        ),
        'date' => array(
          'weight' => '0',
        ),
        'options' => array(
          'weight' => '95',
        ),
        'status' => array(
          'weight' => '0',
        ),
        'promote' => array(
          'weight' => '1',
        ),
        'sticky' => array(
          'weight' => '2',
        ),
        'actions' => array(
          'weight' => '100',
        ),
        'submit' => array(
          'weight' => '5',
        ),
        'preview' => array(
          'weight' => '10',
        ),
        'delete' => array(
          'weight' => '15',
        ),
        'comment_settings' => array(
          'weight' => '30',
        ),
        'comment' => array(
          'weight' => '0',
        ),
        'menu' => array(
          'weight' => '-2',
        ),
        'enabled' => array(
          'weight' => '0',
        ),
        'link' => array(
          'weight' => '1',
        ),
        'link_title' => array(
          'weight' => '0',
        ),
        'description' => array(
          'weight' => '1',
        ),
        'parent' => array(
          'weight' => '2',
        ),
        'weight' => array(
          'weight' => '3',
        ),
        'path' => array(
          'weight' => '30',
        ),
        'alias' => array(
          'weight' => '0',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_country';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_country';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_country';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_country';
  $strongarm->value = '0';
  $export['node_preview_country'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_country';
  $strongarm->value = 0;
  $export['node_submitted_country'] = $strongarm;

  return $export;
}
