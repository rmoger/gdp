<?php
/**
 * @file
 * country.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function country_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tab1_treaties|node|country|gdp_admin';
  $field_group->group_name = 'group_country_tab1_treaties';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'gdp_admin';
  $field_group->parent_name = 'group_country_tabs1';
  $field_group->data = array(
    'label' => 'Treaties',
    'weight' => '20',
    'children' => array(
      0 => 'field_gdp_sub_regional_treaty',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_country_tab1_treaties|node|country|gdp_admin'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1_detention|node|country|default';
  $field_group->group_name = 'group_country_tabs1_detention';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_country_tabs_1';
  $field_group->data = array(
    'label' => 'Detention',
    'weight' => '5',
    'children' => array(
      0 => 'field_max_length_of_detentio',
      1 => 'field_average_length_of_deten',
      2 => 'field_provision_of_information',
      3 => 'field_non_custodial_measures',
      4 => 'field_mandatory_detention',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_country_tabs1_detention|node|country|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1_detention|node|country|form';
  $field_group->group_name = 'group_country_tabs1_detention';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_country_tabs1';
  $field_group->data = array(
    'label' => 'Detention Centres',
    'weight' => '18',
    'children' => array(
      0 => 'field_average_length_of_deten',
      1 => 'field_mandatory_detention',
      2 => 'field_max_length_of_detentio',
      3 => 'field_non_custodial_measures',
      4 => 'field_provision_of_information',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Detention Centres',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_country_tabs1_detention|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1_facilities|node|country|default';
  $field_group->group_name = 'group_country_tabs1_facilities';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_country_tabs_1';
  $field_group->data = array(
    'label' => 'Detention Facilities',
    'weight' => '7',
    'children' => array(
      0 => 'field_detention_centre',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_country_tabs1_facilities|node|country|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1_facilities|node|country|form';
  $field_group->group_name = 'group_country_tabs1_facilities';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_country_tabs1';
  $field_group->data = array(
    'label' => 'Detention Facilities',
    'weight' => '20',
    'children' => array(
      0 => 'field_detention_centre',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_country_tabs1_facilities|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1_legislation|node|country|default';
  $field_group->group_name = 'group_country_tabs1_legislation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_country_tabs_1';
  $field_group->data = array(
    'label' => 'Legislation',
    'weight' => '6',
    'children' => array(
      0 => 'field_constitutional_guarantees',
      1 => 'field_core_national_legislation',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_country_tabs1_legislation|node|country|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1_legislation|node|country|form';
  $field_group->group_name = 'group_country_tabs1_legislation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_country_tabs1';
  $field_group->data = array(
    'label' => 'Legislation',
    'weight' => '19',
    'children' => array(
      0 => 'field_constitutional_guarantees',
      1 => 'field_core_national_legislation',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_country_tabs1_legislation|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1_treaties|node|country|default';
  $field_group->group_name = 'group_country_tabs1_treaties';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_country_tabs_1';
  $field_group->data = array(
    'label' => 'Treaties',
    'weight' => '4',
    'children' => array(
      0 => 'field_gdp_international_treaty',
      1 => 'field_gdp_sub_regional_treaty',
      2 => 'field_gdp_visits_spec_proc_hrc',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Treaties',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_country_tabs1_treaties|node|country|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1|node|country|form';
  $field_group->group_name = 'group_country_tabs1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Country Tabs',
    'weight' => '16',
    'children' => array(
      0 => 'group_country_tabs1_detention',
      1 => 'group_country_tabs1_facilities',
      2 => 'group_country_tabs1_legislation',
      3 => 'group_gdp_country_tabs1_treaties',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_country_tabs1|node|country|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs1|node|country|gdp_admin';
  $field_group->group_name = 'group_country_tabs1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'gdp_admin';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Country_tabs1',
    'weight' => '4',
    'children' => array(
      0 => 'group_country_tab1_treaties',
      1 => 'group_country_tabs2',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_country_tabs1|node|country|gdp_admin'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs2|node|country|gdp_admin';
  $field_group->group_name = 'group_country_tabs2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'gdp_admin';
  $field_group->parent_name = 'group_country_tabs1';
  $field_group->data = array(
    'label' => 'Country Detention Tab',
    'weight' => '21',
    'children' => array(
      0 => 'field_max_length_of_detentio',
      1 => 'field_average_length_of_deten',
      2 => 'field_provision_of_information',
      3 => 'field_mandatory_detention',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_country_tabs2|node|country|gdp_admin'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_country_tabs_1|node|country|default';
  $field_group->group_name = 'group_country_tabs_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'country_tabs_1',
    'weight' => '2',
    'children' => array(
      0 => 'group_country_tabs1_treaties',
      1 => 'group_country_tabs1_detention',
      2 => 'group_country_tabs1_legislation',
      3 => 'group_country_tabs1_facilities',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_country_tabs_1|node|country|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_gdp_country_tabs1_treaties|node|country|form';
  $field_group->group_name = 'group_gdp_country_tabs1_treaties';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_country_tabs1';
  $field_group->data = array(
    'label' => 'Treaties',
    'weight' => '17',
    'children' => array(
      0 => 'field_gdp_international_treaty',
      1 => 'field_gdp_sub_regional_treaty',
      2 => 'field_gdp_visits_spec_proc_hrc',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_gdp_country_tabs1_treaties|node|country|form'] = $field_group;

  return $export;
}
