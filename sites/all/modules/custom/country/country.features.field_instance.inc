<?php
/**
 * @file
 * country.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function country_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-country-body'
  $field_instances['node-country-body'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'gdp_admin' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'ds_code' => 0,
          'filtered_html' => 'filtered_html',
          'full_html' => 0,
          'plain_text' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'ds_code' => array(
              'weight' => 12,
            ),
            'filtered_html' => array(
              'weight' => 0,
            ),
            'full_html' => array(
              'weight' => 1,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
          ),
        ),
      ),
      'display_summary' => 0,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-country-field_average_length_of_deten'
  $field_instances['node-country-field_average_length_of_deten'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 9,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_average_length_of_deten',
    'label' => 'Average Length Of Deten',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(),
      'type' => 'gdp_textfield_widget',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-country-field_constitutional_guarantees'
  $field_instances['node-country-field_constitutional_guarantees'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 6,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_constitutional_guarantees',
    'label' => 'Constitutional Guarantees',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(),
      'type' => 'gdp_yesno_widget',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-country-field_core_national_legislation'
  $field_instances['node-country-field_core_national_legislation'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 7,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_core_national_legislation',
    'label' => 'Core pieces of national legislation',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(),
      'type' => 'gdp_textfield_widget',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-country-field_detention_centre'
  $field_instances['node-country-field_detention_centre'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_detention_centre',
    'label' => 'Detention Centre',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(
        'attached_view' => array(
          'enabled' => 0,
          'key_value' => 'nid',
          'label_value' => 'nid',
          'view_name' => 'treaty_lookup_view--regional_treaties',
        ),
      ),
      'type' => 'gdp_reference_select_widget',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'node-country-field_gdp_international_treaty'
  $field_instances['node-country-field_gdp_international_treaty'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 1,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_gdp_international_treaty',
    'label' => 'International Treaty',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(
        'attached_view' => array(
          'enabled' => 1,
          'key_value' => 'nid',
          'label_value' => 'node_title',
          'view_name' => 'treaty_lookup_view--international_treaties',
        ),
      ),
      'type' => 'gdp_reference_select_widget',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-country-field_gdp_sub_regional_treaty'
  $field_instances['node-country-field_gdp_sub_regional_treaty'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 2,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_gdp_sub_regional_treaty',
    'label' => 'Sub Regional Treaty',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(
        'attached_view' => array(
          'enabled' => 1,
          'key_value' => 'nid',
          'label_value' => 'node_title',
          'view_name' => 'treaty_lookup_view--regional_treaties',
        ),
      ),
      'type' => 'gdp_reference_select_widget',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-country-field_gdp_subregion_ref'
  $field_instances['node-country-field_gdp_subregion_ref'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_gdp_subregion_ref',
    'label' => 'Sub Region',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-country-field_gdp_visits_spec_proc_hrc'
  $field_instances['node-country-field_gdp_visits_spec_proc_hrc'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Visits by Special Procedures of the Human Rights Council.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 3,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_gdp_visits_spec_proc_hrc',
    'label' => 'Vists by Special Procedure of Human Rights Council',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(
        'allowed_values' => array(
          'Working Group on arbitrary detention  ' => 'Working Group on arbitrary detention  ',
          'Special Rapporteur on the promotion and protection of human rights while countering terrorism ' => ' Special Rapporteur on the promotion and protection of human rights while countering terrorism',
          'Special Rapporteur on extrajudicial, summary or arbitrary executions ' => 'Special Rapporteur on extrajudicial, summary or arbitrary executions ',
          'Special Rapporteur on the right of everyone to the enjoyment of the highest attainable standard of physical and mental health ' => ' Special Rapporteur on the right of everyone to the enjoyment of the highest attainable standard of physical and mental health',
          'Special Rapporteur on the human rights of migrants ' => ' Special Rapporteur on the human rights of migrants',
          'Special Rapporteur on contemporary forms of racism, racial discrimination, xenophobia and related intolerance ' => ' Special Rapporteur on contemporary forms of racism, racial discrimination, xenophobia and related intolerance',
          'A new visit' => 'A new visit',
        ),
        'allowed_values_function' => '',
      ),
      'type' => 'gdp_select_widget',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-country-field_mandatory_detention'
  $field_instances['node-country-field_mandatory_detention'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 12,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_mandatory_detention',
    'label' => 'Mandatory detention',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(
        'allowed_values' => array(
          'Yes' => array(
            'All apprehended non-citizens who do not have proper documentation ' => ' All apprehended non-citizens who do not have proper documentation',
            'All asylum seekers ' => 'All asylum seekers ',
            'Non-citizens who have been placed in removal proceedings ' => ' Non-citizens who have been placed in removal proceedings',
            'Non-citizens who have violated a re-entry ban ' => ' Non-citizens who have violated a re-entry ban',
            'Persons who request asylum after being placed in removal proceedings ' => ' Persons who request asylum after being placed in removal proceedings',
            'Persons who request asylum after entry  ' => 'Persons who request asylum after entry  ',
            'Persons who request asylum upon arrival at a port of entry ' => 'Persons who request asylum upon arrival at a port of entry ',
          ),
          'No' => array(
            'No Mandatory Detention' => 'No Mandatory Detention',
            'Persons who request asylum upon arrival at a port of entry ' => 'Persons who request asylum upon arrival at a port of entry ',
          ),
        ),
        'allowed_values_function' => '',
      ),
      'type' => 'gdp_twolevels_select_widget',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'node-country-field_max_length_of_detentio'
  $field_instances['node-country-field_max_length_of_detentio'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 10,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_max_length_of_detentio',
    'label' => 'Maximum Length Of Detention',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(),
      'type' => 'gdp_textfield_widget',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-country-field_non_custodial_measures'
  $field_instances['node-country-field_non_custodial_measures'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 11,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_non_custodial_measures',
    'label' => 'Non-Custodial Measures Adopted',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(
        'allowed_values' => array(
          'Release on bail ' => 'Release on bail ',
          'Provision of a guarantor ' => ' Provision of a guarantor',
          'Supervised release ' => ' Supervised release',
          'Electronic monitoring ' => ' Electronic monitoring',
          'Home detention  ' => ' Home detention',
          'New Measure' => 'New Measure',
          'Some other thing' => 'Some other thing',
        ),
        'allowed_values_function' => '',
      ),
      'type' => 'gdp_select_widget',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'node-country-field_provision_of_information'
  $field_instances['node-country-field_provision_of_information'] = array(
    'bundle' => 'country',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 13,
      ),
      'gdp_admin' => array(
        'label' => 'above',
        'module' => 'gdp_fields',
        'settings' => array(
          'observation_date_format' => '',
          'ratification_date_format' => '',
        ),
        'type' => 'gdp_default',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_provision_of_information',
    'label' => 'Provision of information to detainees',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'gdp_fields',
      'settings' => array(),
      'type' => 'gdp_yesno_widget',
      'weight' => 13,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Average Length Of Deten');
  t('Body');
  t('Constitutional Guarantees');
  t('Core pieces of national legislation');
  t('Detention Centre');
  t('International Treaty');
  t('Mandatory detention');
  t('Maximum Length Of Detention');
  t('Non-Custodial Measures Adopted');
  t('Provision of information to detainees');
  t('Sub Region');
  t('Sub Regional Treaty');
  t('Visits by Special Procedures of the Human Rights Council.');
  t('Vists by Special Procedure of Human Rights Council');

  return $field_instances;
}
