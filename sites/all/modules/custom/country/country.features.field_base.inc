<?php
/**
 * @file
 * country.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function country_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'body'
  $field_bases['body'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'node',
    ),
    'field_name' => 'body',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_average_length_of_deten'
  $field_bases['field_average_length_of_deten'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_average_length_of_deten',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'field_constitutional_guarantees'
  $field_bases['field_constitutional_guarantees'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_constitutional_guarantees',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  // Exported field_base: 'field_core_national_legislation'
  $field_bases['field_core_national_legislation'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_core_national_legislation',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_textfield',
  );

  // Exported field_base: 'field_detention_centre'
  $field_bases['field_detention_centre'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_detention_centre',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'gdp_referenced_entity' => 'detention_centre',
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_reference',
  );

  // Exported field_base: 'field_gdp_international_treaty'
  $field_bases['field_gdp_international_treaty'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_gdp_international_treaty',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'gdp_linked_field' => '',
      'gdp_referenced_entity' => 'treaty',
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_reference',
  );

  // Exported field_base: 'field_gdp_sub_regional_treaty'
  $field_bases['field_gdp_sub_regional_treaty'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_gdp_sub_regional_treaty',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'gdp_linked_field' => '',
      'gdp_referenced_entity' => 'treaty',
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_reference',
  );

  // Exported field_base: 'field_gdp_subregion_ref'
  $field_bases['field_gdp_subregion_ref'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_gdp_subregion_ref',
    'foreign keys' => array(
      'node' => array(
        'columns' => array(
          'target_id' => 'nid',
        ),
        'table' => 'node',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'sub_region' => 'sub_region',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_gdp_visits_spec_proc_hrc'
  $field_bases['field_gdp_visits_spec_proc_hrc'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_gdp_visits_spec_proc_hrc',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'gdp_linked_field' => '',
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 1,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'field_mandatory_detention'
  $field_bases['field_mandatory_detention'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_mandatory_detention',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_twolevels_select',
  );

  // Exported field_base: 'field_max_length_of_detentio'
  $field_bases['field_max_length_of_detentio'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_max_length_of_detentio',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_integer',
  );

  // Exported field_base: 'field_non_custodial_measures'
  $field_bases['field_non_custodial_measures'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_non_custodial_measures',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_select',
  );

  // Exported field_base: 'field_provision_of_information'
  $field_bases['field_provision_of_information'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_provision_of_information',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'gdp_fields',
    'settings' => array(
      'observation_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'ratification_date' => array(
        'format' => 'Y-m-d',
        'year_range' => '-40:+1',
      ),
      'show_ratification_date' => 0,
    ),
    'translatable' => 0,
    'type' => 'gdp_boolean',
  );

  return $field_bases;
}
